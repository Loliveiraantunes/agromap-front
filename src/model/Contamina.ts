import { Doenca } from './Doenca';
import { Cultura } from './Cultura';

export class Contamina{
  
  public id:number;
  public doenca:Doenca;
  public cultura:Cultura;

   constructor(Obj:any){
     if(Obj != null){
      this.id = Obj.id;
      this.doenca = Obj.doenca;
      this.cultura = Obj.cultura;
     }
   }

}