import { Pessoa } from './Pessoa';

export class User{

  public id:number;
  public email:string;
  public login:string;
  public password:string;
  public role:string;
  public pessoa:Pessoa;

  constructor(Obj:any){
    if(Obj != null){
      this.id = Obj.id;
      this.email = Obj.email;
      this.login= Obj.login;
      this.password = Obj.password;
      this.role = Obj.role;
      this.pessoa = Obj.pessoa;
    }
    
  }



}