export class Tratamento {

  public id: number;
  public titulo:string;
  public descricao:string;

  constructor(Obj: any) {
    if (Obj != null) {
      this.id = Obj.id;
      this.titulo = Obj.titulo;
      this.descricao = Obj.descricao;
    }
  }

}