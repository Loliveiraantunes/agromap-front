import { Cultura } from './Cultura';
import { Propriedade } from './Propriedade';
import { Ticket } from './Ticket';

export class Cultivo{
  
  public id:number;
  public cultura:Cultura;
  public dataInicio:Date;
  public dataFim:Date;
  public propriedade:Propriedade;
  public ticket:Ticket;

   constructor(Obj:any){
     if(Obj != null){
      this.id = Obj.id;
      this.dataInicio = Obj.dataInicio;
      this.dataFim = Obj.dataFim;
      this.cultura = Obj.cultura;
      this.propriedade = Obj.propriedade;
      this.ticket = Obj.ticket;
     }
   }

}