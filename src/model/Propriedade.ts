import { User } from './User';
import { Cultivo } from './Cultivo';

export class Propriedade{

  public id:number;
  public logradouro:string;
  public cep:string;
  public cidade:string;
  public bairro:string;
  public uf:string;
  public complemento:string;
  public latitude:string;
  public longitude:string;
  public documento:string;
  public cadastro:Date;
  public numero:string;
  public kmQuadrados:string;
  public user:User;
  public cultivo:Cultivo;

  constructor(Obj:any){
    if(Obj != null){
      if(Obj.id != null){
        this.id = Obj.id;
      }
      this.logradouro = Obj.logradouro;
      this.cep = Obj.cep;
      this.cidade = Obj.cidade;
      this.bairro = Obj.bairro;
      this.uf = Obj.uf;
      this.complemento = Obj.complemento;
      this.latitude = Obj.latitude;
      this.longitude = Obj.longitude;
      this.documento = Obj.documento;
      this.kmQuadrados = Obj.kmQuadrados;
      this.numero = Obj.numero;
      this.cadastro = Obj.cadastro;
      this.user = Obj.user;
      this.cultivo = Obj.cultivo;
    }
  }



}