import { Ticket } from './Ticket';
import { Tratamento } from './Tratamento';

export class Solucao {

  public id: number;
  public descricao:string;
  public ticket:Ticket;
  public tratamento:Tratamento;

  constructor(Obj: any) {
    if (Obj != null) {
      this.id = Obj.id;
      this.descricao = Obj.descricao;
      this.ticket = Obj.ticket;
      this.tratamento = Obj.tratamento;
    }
  }

}