import { User } from './User';

export class Pessoa{
  
  public id:number;
  public nome:string;
  public sobrenome:string;
  public cpf:string;
  public sexo:string;
  public nascimento:Date;
  public user:User;

   constructor(Obj:any){
     if(Obj != null){
      this.id = Obj.id;
      this.nome = Obj.nome;
      this.sobrenome = Obj.sobrenome;
      this.sexo = Obj.sexo;
      this.nascimento = Obj.nascimento; 
      this.cpf = Obj.cpf;
      this.user = Obj.user;
     }
   }

}