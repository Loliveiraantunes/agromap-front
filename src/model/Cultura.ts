export class Cultura{
  
  public id:number;
  public nome:string;
  public tipo:string;
  public descricao:string;

   constructor(Obj:any){
     if(Obj != null){
      this.id = Obj.id;
      this.nome = Obj.nome;
      this.tipo = Obj.tipo;
      this.descricao = Obj.descricao;
     }
   }

}