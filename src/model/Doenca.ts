export class Doenca{
  
  public id:number;
  public nome:string;
  public causador:string;
  public descricao:string;
  public foto:string;


   constructor(Obj:any){
    if(Obj != null){
      this.id = Obj.id;
      this.nome = Obj.nome;
      this.causador = Obj.causador;
      this.descricao = Obj.descricao;
      this.foto = Obj.foto;
    }
   }

}