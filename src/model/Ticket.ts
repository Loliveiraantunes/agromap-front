import { Cultivo } from './Cultivo';
import { Tratamento } from './Tratamento';
import { Contamina } from './Contamina';
import { Solucao } from './Solucao';

export class Ticket {

  public id: number;
  public status: number;
  public descricao:string;
  public img1:File;
  public img2:File;
  public img3:File;
  public dataCriacao:Date;
  public dataFechamento:Date;
  public cultivo:Cultivo;
  public contamina:Contamina;
  public tratamento:Tratamento;
  public solucao:Solucao[];

  constructor(Obj: Ticket) {
    if (Obj != null) {
      this.id = Obj.id;
      this.status = Obj.status;
      this.img1 = Obj.img1;
      this.img2 = Obj.img2;
      this.img3 = Obj.img3;
      this.descricao = Obj.descricao;
      this.dataCriacao = Obj.dataCriacao;
      this.dataFechamento = Obj.dataFechamento;
      this.cultivo = Obj.cultivo;
      this.contamina = Obj.contamina;
      this.tratamento = Obj.tratamento;
      this.solucao = Obj.solucao;
    }
  }

}