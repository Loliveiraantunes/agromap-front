import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, ToastController, NavController } from '@ionic/angular';
import { Util } from 'src/util/Util';
import { User } from 'src/model/User';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CadastrarModalComponent } from '../cadastrar-modal/cadastrar-modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public form:FormGroup;
  public user:User;

  constructor(public menuCtrl: MenuController,
    public formBuilder: FormBuilder,
     public http:HttpClient,
     public modalController: ModalController,
     public toastController: ToastController,
     public navCtrl: NavController) {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['',Validators.required]
    });

   
  }
  

  async showModal(){
    const modal = await this.modalController.create({
      component: CadastrarModalComponent
    });
    await modal.present();
  }

  
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Logado.',
      duration: 2000
    });
    toast.present();
  }

    
  async error404() {
    const toast = await this.toastController.create({
      message: 'usuário ou senha incorreta.',
      duration: 2000
    });
    toast.present();
  }

  submit(){
      this.user = new User(this.form.value);
      this.user.login = this.form.value.email;
      this.http.post(Util.url+"/api/user/login",this.user).subscribe( resp =>{
      let userJS = new User(resp);
      Util.loggin(userJS);
      this.presentToast();
     // this.navCtrl.navigateRoot('/home');
        location.reload();
  
    },error =>{
      this.error404();
    });
  }
 
  ngOnInit() {
    this.menuCtrl.enable(false);
    
    if( Util.logged().id != null){
      this.menuCtrl.enable(true);
      this.navCtrl.navigateRoot('/home');
     
    }
  }

  

}
