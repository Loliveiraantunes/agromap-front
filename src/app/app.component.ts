import { Component, AfterContentInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { User } from 'src/model/User';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements AfterContentInit {

  public appPages;


  private user: User;

  public nome: string
  public sobrenome: string;
  public img;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    let times = 0;
 
    const timer = setInterval(() => {

      if (localStorage.getItem('user_id') != null) {
        this.user = new User(JSON.parse(localStorage.getItem('user_id')));
        this.nome = this.user.pessoa.nome;
        this.sobrenome = this.user.pessoa.sobrenome;
      }

      if (localStorage.getItem('img') != null) {
        this.img = "assets/icon/" + localStorage.getItem('img');
      }


      if(this.appPages == null){
        this.appPages = this.getRole();
      }
      
    }, 200);
    this.initializeApp();

  }


  getRole() {
    let admin = [
      {
        title: 'Home',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'Propriedade',
        url: '/propriedade',
        icon: 'pin'
      },
      {
        title: 'Enfermidades',
        url: '/doenca',
        icon: 'ios-bug'
      },
      {
        title: 'Cultura',
        url: '/cultura',
        icon: 'ios-leaf'
      },
      {
        title: 'Ocorrências',
        url: '/ocorrencia',
        icon: 'ios-alert'
      },
      {
        title: 'Usuários',
        url: '/usuarios',
        icon: 'ios-contact'
      }
    ];

    let user = [
      {
        title: 'Home',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'Propriedade',
        url: '/propriedade',
        icon: 'pin'
      }
    ];

    let lab = [
      {
        title: 'Home',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'Enfermidades',
        url: '/doenca',
        icon: 'ios-bug'
      },
      {
        title: 'Cultura',
        url: '/cultura',
        icon: 'ios-leaf'
      },
      {
        title: 'Ocorrências',
        url: '/ocorrencia',
        icon: 'ios-alert'
      }
    ];


    switch (localStorage.getItem('role')) {
      case 'USER':
        return user;

      case 'ADMIN':
        return admin;

      case 'LAB':
        return lab;
    }



  }


  ngAfterContentInit() {
   

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
