import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoencaPage } from './doenca.page';

describe('DoencaPage', () => {
  let component: DoencaPage;
  let fixture: ComponentFixture<DoencaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoencaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoencaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
