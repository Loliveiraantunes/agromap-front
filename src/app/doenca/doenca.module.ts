import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { IonicModule } from '@ionic/angular';

import { DoencaPage } from './doenca.page';
import { HttpClientModule } from '@angular/common/http';
import { ShowModalModule } from '../show-modal/show-modal.module';

const routes: Routes = [
  {
    path: '',
    component: DoencaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    ShowModalModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers:[SplashScreen],
  declarations: [DoencaPage]
})
export class DoencaPageModule {}
