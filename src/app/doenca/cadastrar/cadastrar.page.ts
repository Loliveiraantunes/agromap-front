import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { Doenca } from 'src/model/Doenca';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.page.html',
  styleUrls: ['./cadastrar.page.scss'],
})
export class CadastrarPage implements OnInit {

  public form: FormGroup;
  public doenca:Doenca = new Doenca(null); 
  public id:any;

  constructor( public NavCtrl:NavController, 
    public formBuilder: FormBuilder, 
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public route:Router) 
  {
    this.form = this.formBuilder.group({
      nome:['',Validators.required],
      causador:['',Validators.required],
      descricao:['']
    });

    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    if(this.id != null){
      this.http.get(Util.url+"/api/doenca/"+this.id).subscribe( resp =>{
        this.doenca = new Doenca(resp);
      });
    }

   }
   async invalid() {
    const toast = await this.toastController.create({
      message: 'Verifique se os campos estão preenchidos corretamente',
      duration: 2000
    });
    toast.present();
  }

  ngOnInit() {

  }

  salvar(){
  
    if(!this.form.valid){
      this.invalid();
      return;
    }
    if(this.id == null){
      this.doenca = new Doenca(this.form.value);
      this.http.post(Util.url+"/api/doenca/add",this.doenca).subscribe( resp =>{
        this.presentToast() ;
        this.NavCtrl.navigateRoot('doenca');
      });
    }else{
      this.http.put(Util.url+"/api/doenca/update",this.doenca).subscribe( resp =>{
        this.update() ;
        this.route.navigate(['/doenca']);
      });
    }
   
  }

  back(){
    this.NavCtrl.back();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Enfermidade Cadastrada com Sucesso',
      duration: 2000
    });
    toast.present();
  }

  async update() {
    const toast = await this.toastController.create({
      message: 'Enfermidade atualizada com Sucesso',
      duration: 2000
    });
    toast.present();
  }

  submit(){
    
  }
}
