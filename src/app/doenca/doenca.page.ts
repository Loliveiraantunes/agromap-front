import { Component, OnInit, AfterContentInit } from '@angular/core';
import { NavController, AlertController, ModalController } from '@ionic/angular';
import { Doenca } from 'src/model/Doenca';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { ActivatedRoute } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ShowModalComponent } from '../show-modal/show-modal.component';

@Component({
  selector: 'app-doenca',
  templateUrl: './doenca.page.html',
  styleUrls: ['./doenca.page.scss'],
})
export class DoencaPage{

  doencas: Doenca[] = new Array();


  constructor(public NavCtrl: NavController,
    public http: HttpClient,
    private alertController: AlertController,
    private splashScreen: SplashScreen,
    public modalController: ModalController,
    private route: ActivatedRoute) {

    this.route.params.subscribe(resp => {
      this.reload();
    });

  }


  async visualizar(doenca) {
    const modal = await this.modalController.create({
      component: ShowModalComponent,
      componentProps: {
        'doenca':doenca
      }
    });
    await modal.present();
  }

  cadastrar() {
    this.NavCtrl.navigateForward('doenca/cadastrar');
  }

  editar(id) {
    this.NavCtrl.navigateForward('doenca/cadastrar/' + id);
  }


  private reload() {
    this.doencas = new Array();
    this.http.get(Util.url + "/api/doenca/all").subscribe((resp: Doenca[]) => {
      resp.forEach((doenca) => {
        this.doencas.push(new Doenca(doenca));
        //this.splashScreen.hide();
      });
    });
  }

  async deletar(id, nome) {
    const alert = await this.alertController.create({
      header: 'Excluir!',
      message: 'Deseja deletar "' + nome + '"? Após a Confirmação não é possível recuperar o registro.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Deletar',
          handler: () => {

            this.http.delete(Util.url + "/api/doenca/remove/" + id).subscribe((resp) => {
              this.reload()
            });
          }
        }
      ]
    });
    await alert.present();
  }

}
