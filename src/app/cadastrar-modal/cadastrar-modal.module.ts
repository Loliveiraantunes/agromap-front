import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { CadastrarModalComponent } from './cadastrar-modal.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports:
  [ 
    CommonModule, 
    IonicModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule
  ],
  declarations:
  [
    CadastrarModalComponent
  ],
  entryComponents:[
    CadastrarModalComponent
  ],
  exports:[
    CadastrarModalComponent
  ]
})
export class CadastrarModalModule{}