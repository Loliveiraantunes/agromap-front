import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/model/User';
import { Pessoa } from 'src/model/Pessoa';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';

@Component({
  selector: 'app-cadastrar-modal',
  templateUrl: './cadastrar-modal.component.html',
  styleUrls: ['./cadastrar-modal.component.scss'],
})
export class CadastrarModalComponent implements OnInit {

  public form: FormGroup;
  public user: User;
  public pessoa: Pessoa;

  constructor(
    public menuCtrl: MenuController,
    public formBuilder: FormBuilder,
    public http: HttpClient,
    public modal: ModalController,
    public toastController: ToastController) {

    this.form = this.formBuilder.group({
      login: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      copassword: ['', Validators.required],
      nome: ['', Validators.required],
      sobrenome: ['', Validators.required],
      cpf: ['', Validators.required],
      sexo: ['', Validators.required],
      nascimento: ['', Validators.required],
    });

  }

  submit(){
    this.cadastrarUser();
  }

  cadastrarUser(){
    this.user = new User(this.form.value);
    this.pessoa = new Pessoa(this.form.value);

    if(!this.form.valid){
      this.invalido();
      return;
    }

    if(this.form.value.password == this.form.value.copassword){
      this.http.post(Util.url+"/api/user/add",this.user).subscribe( (resp:Response) =>{
        let userJS = new User(resp);
        this.pessoa.user = userJS;
        this.cadastrarPessoa(this.pessoa);
    });
    }else{
      this.presentToastPassword();
      return;
    }
  
  }
  cadastrarPessoa(pessoa){
    this.http.post(Util.url+"/api/pessoa/add",pessoa).subscribe( resp =>{
      console.log(resp);
      this.dismiss(); 
      this.presentToast() 
    });
  }
  async presentToastPassword() {
    const toast = await this.toastController.create({
      message: 'As senhas não coincidem.',
      duration: 2000
    });
    toast.present();
  }

  async invalido() {
    const toast = await this.toastController.create({
      message: 'Verifique se todos os campos foram preenchidos corretamente.',
      duration: 2000
    });
    toast.present();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Sua conta foi criada com sucesso.',
      duration: 2000
    });
    toast.present();
  }
  dismiss() {
    this.modal.dismiss();
  }
  ngOnInit() { }

}
