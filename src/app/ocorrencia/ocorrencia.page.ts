import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { Ticket } from 'src/model/Ticket';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-ocorrencia',
  templateUrl: './ocorrencia.page.html',
  styleUrls: ['./ocorrencia.page.scss'],
})
export class OcorrenciaPage implements OnInit {

  public tickets:Ticket[] = new Array();
  public ticket:Ticket;
  
  constructor(
    public http:HttpClient,
    public NavCtrl:NavController
  ) {

   }

  ngOnInit() {
    this.http.get(Util.url + "/api/ticket/all").subscribe((resp: Ticket[]) => {
      resp.forEach((ticket) => {
        this.tickets.push(new Ticket(ticket));
      });
    });
  }

  visualizar(id){
    this.NavCtrl.navigateRoot('ocorrencia/analisar/'+id);
  }


  submit(){
    
  }
}
