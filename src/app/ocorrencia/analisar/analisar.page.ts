import { Component, OnInit } from '@angular/core';
import { Util } from 'src/util/Util';
import { Cultura } from 'src/model/Cultura';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Ticket } from 'src/model/Ticket';
import { Contamina } from 'src/model/Contamina';
import { Cultivo } from 'src/model/Cultivo';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ToastController, NavController, AlertController } from '@ionic/angular';
import { Doenca } from 'src/model/Doenca';
import { Tratamento } from 'src/model/Tratamento';
import { Solucao } from 'src/model/Solucao';

@Component({
  selector: 'app-analisar',
  templateUrl: './analisar.page.html',
  styleUrls: ['./analisar.page.scss'],
})
export class AnalisarPage implements OnInit {
  public form: FormGroup;
  public ticket: Ticket = new Ticket(null);
  public id: any;
  public cultura: Cultura = new Cultura(null);
  public doenca: Doenca = new Doenca(null);
  public solucoes: Solucao[] = new Array();
  public contaminacao: Contamina[] = new Array();

  constructor(public NavCtrl: NavController,
    public formBuilder: FormBuilder,
    public toastController: ToastController,
    public alertController: AlertController,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public route: Router) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id != null) {
      this.http.get(Util.url + "/api/ticket/" + this.id).subscribe((resp:Ticket) => {
        this.ticket = resp
        this.cultura = this.ticket.contamina.cultura;
        this.doenca = this.ticket.contamina.doenca;
        console.log(resp)
        this.http.get(Util.url + "/api/contamina/cultura/" + this.cultura.id).subscribe((resp: Contamina[]) => {
          resp.forEach((conta) => {
            this.contaminacao.push(new Contamina(conta));
          });
        });
      });


    }

    this.activatedRoute.params.subscribe(resp => {
      this.reload();
    });


  }
  compareById(o1, o2) {
    return o1.id === o2.id
  }

  ticketAtt() {
    this.http.put(Util.url + "/api/ticket/update", this.ticket).subscribe((res:Ticket) => {
      this.ticket = new Ticket(res)
    });
  }

  ngOnInit() {
  }

  reload() {
    this.solucoes = new Array();
    this.http.get(Util.url + "/api/ticket/solucao/" + this.id).subscribe((resp: Solucao[]) => {
      resp.forEach(solucao => {
        this.solucoes.push(new Solucao(solucao));
      });
    });
  }

  salvar() {
    if (!this.form.valid) {
      this.invalid();
      return;
    }
    if (this.id == null) {
      this.ticket.dataCriacao = new Date();
      this.ticket.status = 1;
      this.ticket.contamina = this.form.value.contamina;
      this.ticket.descricao = this.form.value.descricao;
      this.http.post(Util.url + "/api/ticket/add", this.ticket).subscribe((resp) => {
        this.presentToast();
        this.NavCtrl.back()
      });
    } else {
      
      this.http.put(Util.url + "/api/ticket/update", this.ticket).subscribe(resp => {
        this.update();
        this.NavCtrl.back();
      });
    }
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Cadastrado com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  async update() {
    const toast = await this.toastController.create({
      message: 'Atualizado com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  async invalid() {
    const toast = await this.toastController.create({
      message: 'Verifique se os campos estão preenchidos corretamente',
      duration: 2000
    });
    toast.present();
  }

  back() {
    this.NavCtrl.back();
  }

  cadastrar() {
    this.NavCtrl.navigateForward('ocorrencia/tratamento/' + this.id);
  }

  async deletar(id) {
    const alert = await this.alertController.create({
      header: 'Excluir!',
      message: 'Deseja deletar? Após a Confirmação não é possível recuperar o registro.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Deletar',
          handler: () => {
            this.http.delete(Util.url + "/api/solucao/remove/" + id).subscribe((resp) => {
              this.reload()
            });
          }
        }
      ]
    });
    await alert.present();
  }

  atender() {
    this.ticket.status = 2;
    this.ticketAtt();
  }

  finalizar() {
    this.ticket.status = 3;
    this.ticket.dataFechamento = new Date();
    this.ticketAtt();
  }

  cancelar() {
    this.ticket.status = 4;
    this.ticket.dataFechamento = new Date();
    this.ticketAtt();
  }
  

  visualizar(id) {
    this.NavCtrl.navigateForward('ocorrencia/tratamento/' + this.id + '/' + id);
  }

}
