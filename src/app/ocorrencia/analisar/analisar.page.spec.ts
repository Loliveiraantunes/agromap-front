import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisarPage } from './analisar.page';

describe('AnalisarPage', () => {
  let component: AnalisarPage;
  let fixture: ComponentFixture<AnalisarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalisarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
