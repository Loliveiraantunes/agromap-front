import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController, ToastController } from '@ionic/angular';
import { Util } from 'src/util/Util';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Tratamento } from 'src/model/Tratamento';
import { Ticket } from 'src/model/Ticket';
import { Solucao } from 'src/model/Solucao';

@Component({
  selector: 'app-tratamento',
  templateUrl: './tratamento.page.html',
  styleUrls: ['./tratamento.page.scss'],
})
export class TratamentoPage implements OnInit {

  public form: FormGroup;
  public id: any;
  public id_tratamento: any;

  public tratamento:Tratamento = new Tratamento(null);
  public tratamentos:Tratamento[] = new Array();
  public ticket:Ticket = new Ticket(null);
  public solucao:Solucao = new Solucao(null);

  constructor(public NavCtrl: NavController,
    public formBuilder: FormBuilder,
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public route: Router) {

    this.form = this.formBuilder.group({
      titulo:[''],
      descricao:[''],
     // orientacoes:[''],
      tratamento:['']
    });

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.id_tratamento = this.activatedRoute.snapshot.paramMap.get('tratamento');

    if(this.id != null){
      this.http.get(Util.url + "/api/ticket/" + this.id).subscribe((resp:Ticket) => {
        this.ticket = resp
        console.log(resp);
      });
    }

    if (this.id_tratamento != null) {
      this.http.get(Util.url + "/api/tratamento/" + this.id_tratamento).subscribe(resp => {
       this.tratamento = new Tratamento(resp);
       console.log(resp)
      });
    }
  }

  ngOnInit() {
    this.tratamentos = new Array();
    this.http.get(Util.url + "/api/tratamento/all").subscribe((resp: Tratamento[]) => {
      resp.forEach( (tratamento) =>{
        this.tratamentos.push(new Tratamento(tratamento));
      })
    });
  }

  salvar() {
    if (!this.form.valid) {
      this.invalid();
      return;
    }
    if (this.id_tratamento == null) {
     this.tratamento.titulo = this.form.value.titulo;
     this.tratamento.descricao = this.form.value.descricao;
      this.http.post(Util.url + "/api/tratamento/add", this.tratamento).subscribe(resp => {
        this.tratamento = new Tratamento(resp);
        this.solucao.tratamento = this.tratamento;
        this.solucao.ticket = this.ticket;
          this.http.post(Util.url + "/api/solucao/add", this.solucao).subscribe(respSolucao => {
            this.presentToast();
            this.NavCtrl.back();
          });
      });
    } else {
      this.http.put(Util.url + "/api/tratamento/update", this.tratamento).subscribe(resp => {
        this.update();
        this.NavCtrl.back();
      });
    }

  }
  compareById(o1, o2) {
    return o1.id === o2.id
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Registro cadastrado com Sucesso',
      duration: 2000
    });
    toast.present();
  }

  async update() {
    const toast = await this.toastController.create({
      message: 'Registro atualizado com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  back() {
    this.NavCtrl.back();
  }

  async invalid() {
    const toast = await this.toastController.create({
      message: 'Verifique se os campos estão preenchidos corretamente',
      duration: 2000
    });
    toast.present();
  }

  submit(){
    
  }
}
