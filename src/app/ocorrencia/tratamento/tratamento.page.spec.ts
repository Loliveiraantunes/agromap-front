import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TratamentoPage } from './tratamento.page';

describe('TratamentoPage', () => {
  let component: TratamentoPage;
  let fixture: ComponentFixture<TratamentoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TratamentoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TratamentoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
