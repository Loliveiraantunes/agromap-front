import { Component, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { Propriedade } from 'src/model/Propriedade';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { Ticket } from 'src/model/Ticket';
import { Solucao } from 'src/model/Solucao';
declare var google;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements AfterContentInit {

  public propriedades: Propriedade[] = new Array();
  public tickets: Ticket[] = new Array();
  public solucoes: Solucao[] = new Array();
  constructor(public http: HttpClient) { }

  @ViewChild('map') mapElement: ElementRef;
  map: any;


  ionViewDidLoad() {

  }

  toArray(answers: object) {
    return Object.keys(answers).map(key => ({
      key,
      ...answers[key]
    }));
  }

  showPropriedade(propriedade: any) {
    this.tickets = new Array();
    propriedade.cultivo.forEach(ticket => {
      this.tickets.push(ticket)
     
    });
    this.map.setZoom(9);
    this.map.setCenter(new google.maps.LatLng(propriedade.latitude, propriedade.longitude));
  }

  reload() {
    this.http.get(Util.url + "/api/propriedade/markers").subscribe((resp: Propriedade[]) => {
      resp.forEach(prop => {

        this.propriedades.push(new Propriedade(prop));
        this.propriedades.forEach((prop: Propriedade) => {
          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(prop.latitude, prop.longitude),
            data: prop
          });
          marker.setVisible(true);
          marker.setMap(this.map);

          marker.addListener('click', () => {
            this.showPropriedade(marker.data)
          });
        });

      })
    });


  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    this.reload();
    this.loadMap();
  }

  loadMap() {

    this.map = new google.maps.Map(
      this.mapElement.nativeElement, {
        center: { lat: -15.7745457, lng: -48.8093811 },
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        styles: [
          {
            "elementType": "labels",
            "stylers": [
              { "visibility": "off" }
            ]
          }
        ]
      });

    // Set CSS for the control border.

    let controlDiv = document.createElement('div');

    let controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to recenter the map';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Centralizar';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', () => {
      this.map.setCenter({ lat: -15.7745457, lng: -48.8093811 });
      this.map.setZoom(4);
    });

    this.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);

  }
}
