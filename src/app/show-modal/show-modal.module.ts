import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { IonicModule } from '@ionic/angular';

import { HttpClientModule } from '@angular/common/http';
import { ShowModalComponent } from './show-modal.component';

const routes: Routes = [
  {
    path: '',
    component: ShowModalComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations:
  [
    ShowModalComponent
  ],
  entryComponents:[
    ShowModalComponent
  ]
})
export class ShowModalModule {}
