import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Doenca } from 'src/model/Doenca';
import { Cultura } from 'src/model/Cultura';
import { User } from 'src/model/User';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';

@Component({
  selector: 'app-show-modal',
  templateUrl: './show-modal.component.html',
  styleUrls: ['./show-modal.component.scss'],
})
export class ShowModalComponent implements OnInit {

  doenca: Doenca = new Doenca(null);
  cultura: Cultura = new Cultura(null);
  user: User = new User(null);

  constructor(navParams: NavParams, public modal: ModalController, public http: HttpClient) {

    this.doenca = navParams.get('doenca');
    this.cultura = navParams.get('cultura');
    this.user = navParams.get('usuario');

  }

  ngOnInit() {

  }

  dismiss() {
    this.modal.dismiss();
  }

  atualizar() {
  
    this.http.put(Util.url + "/api/user/update", this.user).subscribe(resp => {
      console.log(resp)
      this.modal.dismiss();
    });
  }


}
