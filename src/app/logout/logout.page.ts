import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(public NavCtrl:NavController,public menu:MenuController) {


   }

  ngOnInit() {
    localStorage.clear();
    this.NavCtrl.navigateRoot('');
    this.menu.enable(false);
  }

}
