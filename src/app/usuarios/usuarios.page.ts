import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/model/User';
import { Util } from 'src/util/Util';
import { ShowModalComponent } from '../show-modal/show-modal.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.page.html',
  styleUrls: ['./usuarios.page.scss'],
})
export class UsuariosPage implements OnInit {

  public usuarios: User[] = new Array();

  constructor(public NavCtrl: NavController,
    public http: HttpClient,
    private alertController: AlertController,
    public modalController: ModalController,
    private route: ActivatedRoute) {



  }


  reload() {
    this.http.get(Util.url + "/api/user/all").subscribe((resp: User[]) => {
      resp.forEach(user => {
        this.usuarios.push(user)
      })
    });
  }

  ngOnInit() {
    this.reload();
  }

  async visualizar(usuario) {
    const modal = await this.modalController.create({
      component: ShowModalComponent,
      componentProps: {
        'usuario': usuario
      }
    });
    await modal.present();
  }
}
