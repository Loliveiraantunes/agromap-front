import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaminaPage } from './contamina.page';

describe('ContaminaPage', () => {
  let component: ContaminaPage;
  let fixture: ComponentFixture<ContaminaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaminaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaminaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
