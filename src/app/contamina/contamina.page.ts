import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Contamina } from 'src/model/Contamina';
import { NavController, AlertController, ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ActivatedRoute } from '@angular/router';
import { Util } from 'src/util/Util';

@Component({
  selector: 'app-contamina',
  templateUrl: './contamina.page.html',
  styleUrls: ['./contamina.page.scss'],
})
export class ContaminaPage implements OnInit {
  public form: FormGroup;

  public contaminacao: Contamina[] = new Array();


  constructor(public NavCtrl: NavController,
    public http: HttpClient,
    private alertController: AlertController,
    private splashScreen: SplashScreen,
    private route: ActivatedRoute) {

    this.route.params.subscribe(resp => {
      this.reload();
    });

  }

  ngOnInit() {
  }

  editar(id) {
    this.NavCtrl.navigateForward('contamina/cadastrar/' + id);
  }


  private reload() {
    this.contaminacao = new Array();
    this.http.get(Util.url + "/api/contamina/all").subscribe((resp: Contamina[]) => {
      resp.forEach((contamina) => {
        this.contaminacao.push(new Contamina(contamina));
      });
    });
  }

  async deletar(id, contaminacao) {
    const alert = await this.alertController.create({
      header: 'Excluir!',
      message: 'Deseja deletar "' + contaminacao.cultura.nome + ' e ' + contaminacao.doenca.nome + '"? Após a Confirmação não é possível recuperar o registro.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Deletar',
          handler: () => {

            this.http.delete(Util.url + "/api/contamina/remove/" + id).subscribe((resp) => {
              this.reload()
            });
          }
        }
      ]
    });
    await alert.present();
  }
  cadastrar() {
    this.NavCtrl.navigateForward('contamina/cadastrar');
  }
}
