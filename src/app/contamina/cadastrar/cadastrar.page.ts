import { Component, OnInit } from '@angular/core';
import { Contamina } from 'src/model/Contamina';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { Cultura } from 'src/model/Cultura';
import { Doenca } from 'src/model/Doenca';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.page.html',
  styleUrls: ['./cadastrar.page.scss'],
})
export class CadastrarPage implements OnInit {

  public form: FormGroup;
  public id: any;
  public id_contamina: any;

  contamina: Contamina;
  culturas: Cultura[] = new Array();
  doencas: Doenca[] = new Array();

  public doenca: Doenca = new Doenca(null);
  public cultura: Cultura = new Cultura(null);

  constructor(public NavCtrl: NavController,
    public formBuilder: FormBuilder,
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public route: Router) {

    this.form = this.formBuilder.group({
      cultura: ['', Validators.required],
      doenca: ['', Validators.required]
    });

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.id_contamina = this.activatedRoute.snapshot.paramMap.get('contamina');

    if (this.id_contamina != null) {
      this.http.get(Util.url + "/api/contamina/" + this.id_contamina).subscribe(resp => {
        this.contamina = new Contamina(resp);
        this.doenca = new Doenca(this.contamina.doenca);
      });
    }
  }

  ngOnInit() {
    this.http.get(Util.url + "/api/cultura/"+this.id).subscribe((resp: Cultura) => {
        this.cultura = resp;
    });

    this.doencas = new Array();
    this.http.get(Util.url + "/api/doenca/all").subscribe((resp: Doenca[]) => {
      resp.forEach((doenca) => {
        this.doencas.push(new Doenca(doenca));
      });
    });
  }

  salvar() {

    if (!this.form.valid) {
      this.invalid();
      return;
    }
    if (this.id_contamina == null) {
      this.contamina = new Contamina(this.form.value);
      this.contamina.cultura = this.cultura;
      this.http.post(Util.url + "/api/contamina/add", this.contamina).subscribe(resp => {
        this.presentToast();
        this.NavCtrl.back();
      });
    } else {
      this.contamina.cultura = this.cultura;
      this.contamina.doenca = this.form.value.doenca;

      this.http.put(Util.url + "/api/contamina/update", this.contamina).subscribe(resp => {
        this.update();
        this.NavCtrl.back();
      });
    }

  }
  compareById(o1, o2) {
    return o1.id === o2.id
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Registro cadastrado com Sucesso',
      duration: 2000
    });
    toast.present();
  }

  async update() {
    const toast = await this.toastController.create({
      message: 'Registro atualizado com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  back() {
    this.NavCtrl.back();
  }

  async invalid() {
    const toast = await this.toastController.create({
      message: 'Verifique se os campos estão preenchidos corretamente',
      duration: 2000
    });
    toast.present();
  }
  
  submit(){

  }
}
