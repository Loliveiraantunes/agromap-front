import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Ticket } from 'src/model/Ticket';
import { Cultura } from 'src/model/Cultura';
import { Util } from 'src/util/Util';
import { Cultivo } from 'src/model/Cultivo';
import { Contamina } from 'src/model/Contamina';
import { Doenca } from 'src/model/Doenca';
import { Solucao } from 'src/model/Solucao';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.page.html',
  styleUrls: ['./ticket.page.scss'],
})
export class TicketPage implements OnInit {
  public form: FormGroup;
  public ticket: Ticket = new Ticket(null);
  public id: any;
  public idTicket: any;
  public culturas: Cultura[] = new Array();
  public contaminacao: Contamina[] = new Array();
  public cultivo: Cultivo = new Cultivo(null);
  public solucoes: Solucao[] = new Array();

  @ViewChild('filechooser') fileChooserElementRef: ElementRef;
  items: File[] = [];


  constructor(public NavCtrl: NavController,
    public formBuilder: FormBuilder,
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public route: Router) {


    this.id = this.activatedRoute.snapshot.paramMap.get('idCultivo');

    if (this.id != null) {

      this.http.get(Util.url + "/api/cultivo/" + this.id).subscribe((resp: Cultivo) => {
        this.cultivo = resp;
        this.http.get(Util.url + "/api/contamina/cultura/" + this.cultivo.cultura.id).subscribe((respCont: Contamina[]) => {
          respCont.forEach((conta) => {
            this.contaminacao.push(new Contamina(conta));

          });
        });
      });

      this.http.get(Util.url + "/api/ticket/cultivo/" + this.id).subscribe((resp: Ticket) => {
        this.ticket = new Ticket(resp);
        if (this.ticket.id != null) {
          this.http.get(Util.url + "/api/ticket/solucao/" + this.ticket.id).subscribe((res: Solucao[]) => {
            res.forEach(solucao => {
              this.solucoes.push(new Solucao(solucao));
            })
          });
        }
      });
    }
    this.form = this.formBuilder.group({
      descricao: ['', Validators.required],
      cultura: ['', Validators.required],
      contamina: ['', Validators.required],
      status: ['']
    });
  }

  compareById(o1, o2) {
    return o1.id === o2.id
  }

  ngOnInit() {
    this.culturas = new Array();
    this.http.get(Util.url + "/api/cultura/all").subscribe((resp: Cultura[]) => {
      resp.forEach((cultura) => {
        this.culturas.push(new Cultura(cultura));
      });
    });

  }

  salvar() {
    if (!this.form.valid) {
      this.invalid();
      return;
    }
    if (this.idTicket == null) {
      this.ticket.dataCriacao = new Date();
      this.ticket.status = 1;
      this.ticket.cultivo = this.cultivo;
      this.ticket.contamina = this.form.value.contamina;
      this.ticket.descricao = this.form.value.descricao;

      this.http.post(Util.url + "/api/ticket/add", this.ticket).subscribe((resp) => {
        this.updateCultivo(resp);
      });
    } else {
      this.ticket.cultivo = this.cultivo;

      this.http.put(Util.url + "/api/ticket/update", this.ticket).subscribe(resp => {
        this.updateCultivo(resp);
      });
    }
  }

  updateCultivo(ticket) {
    this.cultivo.ticket = new Ticket(ticket);
    this.http.put(Util.url + "/api/cultivo/update/", this.cultivo).subscribe(resp => {
      this.update();
      this.NavCtrl.back();
    });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Cadastrado com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  async update() {
    const toast = await this.toastController.create({
      message: 'Atualizado com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  async invalid() {
    const toast = await this.toastController.create({
      message: 'Verifique se os campos estão preenchidos corretamente',
      duration: 2000
    });
    toast.present();
  }

  back() {
    this.NavCtrl.back();
  }

  submit() {

  }



  uploadImg1(event) {
    let files: File = event.target.files[0];
    let reader = new FileReader();
    reader.onload = (e: any) => {
      this.ticket.img1 = e.target.result;
    }
    reader.readAsDataURL(files);
  }

  public file2: File;
  uploadImg2(event) {
    let files: File = event.target.files[0];
    let reader = new FileReader();
    reader.onload = (e: any) => {
      this.ticket.img2 = e.target.result;
    }
    reader.readAsDataURL(files);
  }

  public file3: File;
  uploadImg3(event) {
    let files: File = event.target.files[0];
    let reader = new FileReader();
    reader.onload = (e: any) => {
      this.ticket.img3 = e.target.result;
    }
    reader.readAsDataURL(files);
  }
}
