import { Component, OnInit, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { NavController, ToastController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { Propriedade } from 'src/model/Propriedade';
import { Cultivo } from 'src/model/Cultivo';

declare var google;

@Component({
  selector: 'app-visualizar',
  templateUrl: './visualizar.page.html',
  styleUrls: ['./visualizar.page.scss'],
})
export class VisualizarPage implements OnInit, AfterContentInit {

  @ViewChild('map') mapElement: ElementRef;

  map: any;
  marker: any;
  id: any;

  public propriedade:Propriedade = new Propriedade(null);
  public cultivos:Cultivo[] = new Array();

  constructor(public NavCtrl: NavController,
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    public http: HttpClient,
    public route:ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    if (this.id != null) {
      this.http.get(Util.url + "/api/propriedade/" + this.id).subscribe(resp => {
        this.propriedade = new Propriedade(resp);
        this.map.setCenter(new google.maps.LatLng(this.propriedade.latitude, this.propriedade.longitude));
        this.map.setZoom(17);
        this.marker.setPosition(new google.maps.LatLng(this.propriedade.latitude, this.propriedade.longitude));
        this.marker.setVisible(true);
        this.marker.setMap(this.map);
      });

    }

    this.route.params.subscribe(resp => {
      this.reload();
    });
  }

    ngAfterContentInit(){
      this.loadMap();
    }

    ngOnInit() {
    }

    loadMap() {
      this.map = new google.maps.Map(
        this.mapElement.nativeElement, {
          center: { lat: -27.5967945, lng: -48.5538321 },
          zoom: 15,
          draggable:false,
          disableDefaultUI: true
        });

      this.marker = new google.maps.Marker({
        map: this.map,
        draggable: false
      });

    }
    adicionarCultivo(){
      this.NavCtrl.navigateForward('cultivo/'+this.id);
    }
    back(){
      this.NavCtrl.navigateBack('/propriedade');
    }

    ticket(idCultivo){
      this.NavCtrl.navigateForward('ticket/'+idCultivo);
    }

    editar(idCultivo){
      this.NavCtrl.navigateForward('cultivo/'+this.id+"/"+idCultivo);
    }

    async deletar(id, cultivo) {
      const alert = await this.alertController.create({
        header: 'Excluir!',
        message: 'Deseja deletar um Cultivo de "' + cultivo.cultura.nome+'"? Após a Confirmação não é possível recuperar o registro.',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
  
            }
          }, {
            text: 'Deletar',
            handler: () => {
              this.http.delete(Util.url + "/api/cultivo/remove/" + id).subscribe((resp) => {
                this.reload()
              });
            }
          }
        ]
      });
      await alert.present();
    }

    private reload() {
      this.cultivos = new Array();
      this.http.get(Util.url + "/api/cultivo/propriedade/"+this.id).subscribe((resp: Cultivo[]) => {
        resp.forEach((cultivo) => {
          this.cultivos.push(new Cultivo(cultivo));
        });
      });
    }
  }
