import { Component, OnInit, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Propriedade } from 'src/model/Propriedade';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
declare var google;
@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.page.html',
  styleUrls: ['./cadastrar.page.scss'],
})
export class CadastrarPage implements OnInit, AfterContentInit {


  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('autocomplete') autocompleteElement: ElementRef;
  @ViewChild('infoWindow') infoWindowElement: ElementRef;

  map: any;
  marker: any;
  infowindow: any;
  infowindowContent: any;
  autocomplete: any;

  id:any;
  geocoder: any;

  public form: FormGroup;
  cep: string;

  propriedade: Propriedade = new Propriedade(null);


  constructor(public NavCtrl:NavController, 
    public formBuilder: FormBuilder, 
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public route:Router) {
    this.form = this.formBuilder.group({
      cep: ['', Validators.required],
      logradouro: ['', Validators.required],
      uf: [''],
      cidade: [''],
      bairro:[''],
      latitude:[''],
      longitude:[''],
      cadastro:[''],
      complemento:[''],
      documento:[''],
      numero:[''],
      kmQuadrados:['',Validators.required]
    });
    
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if(this.id != null){
      this.http.get(Util.url+"/api/propriedade/"+this.id).subscribe( resp =>{
        this.propriedade = new Propriedade(resp);
        this.map.setCenter(new google.maps.LatLng(this.propriedade.latitude, this.propriedade.longitude));
        this.map.setZoom(17);
        this.marker.setPosition(new google.maps.LatLng(this.propriedade.latitude, this.propriedade.longitude));
        this.marker.setVisible(true);
        this.marker.setMap(this.map);
      });
    }

  }

  ngOnInit() {
  }

  salvar(){
    this.propriedade.kmQuadrados = this.form.value.kmQuadrados;
    this.propriedade.user = Util.logged();
    
    if(!this.form.valid){
      this.invalid();
      return;
    }
    if(this.id == null){
      this.propriedade.cadastro = new Date();
      this.propriedade.numero = this.form.value.numero;
      this.http.post(Util.url+"/api/propriedade/add",this.propriedade).subscribe( (resp:Propriedade) =>{
        this.presentToast() ;
        this.NavCtrl.navigateRoot('propriedade/visualizar/'+resp.id);
      });
    }else{
      
      this.http.put(Util.url+"/api/propriedade/update/"+this.id,this.propriedade).subscribe( resp =>{
        this.update() ;
        this.route.navigate(['/propriedade']);
      });
    }
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Propriedade Cadastrada com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  async update() {
    const toast = await this.toastController.create({
      message: 'Propriedade atualizada com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  async invalid() {
    const toast = await this.toastController.create({
      message: 'Verifique se os campos estão preenchidos corretamente',
      duration: 2000
    });
    toast.present();
  }

  back() {
    this.NavCtrl.back();
  }

  ngAfterContentInit(): void {
    this.loadMap();
  }

  loadMap() {

    this.map = new google.maps.Map(
      this.mapElement.nativeElement, {
        center: { lat: -27.5967945, lng: -48.5538321 },
        zoom: 15,
        disableDefaultUI: true
      });

    this.geocoder = new google.maps.Geocoder;

    this.autocomplete = new google.maps.places.Autocomplete(this.autocompleteElement.nativeElement);
    this.autocomplete.bindTo('bounds', this.map);
    this.autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);


    this.marker = new google.maps.Marker({
      map: this.map,
      draggable: true
    });

    this.autocomplete.addListener('place_changed', (resp) => {

      this.propriedade.logradouro ="...";
          this.propriedade.uf ="...";
          this.propriedade.cidade = "...";
          this.propriedade.bairro = "...";
          this.propriedade.cep ="...";
      // this.infowindow.close();
      this.marker.setVisible(false);
      var place = this.autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      if (place.geometry.viewport) {
        this.map.fitBounds(place.geometry.viewport);
      } else {
        this.map.setCenter(place.geometry.location);
        this.map.setZoom(17);
      }
      this.marker.setPosition(place.geometry.location);
      this.marker.setVisible(true);
      var address = '';
      if (place.address_components) {
        this.geocodeLatLng(this.geocoder, this.map, this.marker.getPosition().lat() + "," + this.marker.getPosition().lng());
      }
    });

    google.maps.event.addListener(this.marker, "dragend", (event) => {
      this.propriedade.latitude = event.latLng.lat();
      this.propriedade.longitude = event.latLng.lng();
      this.propriedade.user = Util.logged();
      this.propriedade.cadastro = new Date();
      this.geocodeLatLng(this.geocoder, this.map, (event.latLng.lat() + "," + event.latLng.lng()));
    });
  }

  geocodeLatLng(geocoder, map, latLngCoords) {
    var latlngStr = latLngCoords.split(',', 2);
    var latlng = { lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1]) };
    geocoder.geocode({ 'location': latlng }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {

         // console.log(results);
          if(results[0].address_components.length == 7){
            this.propriedade.logradouro = results[0].address_components[1].long_name;
            this.propriedade.uf = results[0].address_components[4].short_name;
            this.propriedade.cidade = results[0].address_components[3].long_name;
            this.propriedade.bairro = results[0].address_components[2].long_name;
            this.propriedade.cep = results[0].address_components[6].long_name;
            this.propriedade.latitude = latlngStr[0];
            this.propriedade.longitude = latlngStr[1];
          }else if(results[0].address_components.length == 6){
            this.propriedade.logradouro = results[0].address_components[1].long_name;
            this.propriedade.uf = results[0].address_components[3].short_name;
            this.propriedade.cidade = results[0].address_components[2].long_name;
            this.propriedade.bairro = results[0].address_components[2].long_name;
            this.propriedade.cep = results[0].address_components[5].long_name;
            this.propriedade.latitude = latlngStr[0];
            this.propriedade.longitude = latlngStr[1];
          }else  if(results[0].address_components.length == 5){
            this.propriedade.logradouro = results[0].address_components[0].long_name;
            this.propriedade.uf = results[0].address_components[3].short_name;
            this.propriedade.cidade = results[0].address_components[2].long_name;
            this.propriedade.bairro = results[0].address_components[0].long_name;
            this.propriedade.cep = results[0].address_components[4].long_name;
            this.propriedade.latitude = latlngStr[0];
            this.propriedade.longitude = latlngStr[1];
          }
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  submit(){
    
  }
}
