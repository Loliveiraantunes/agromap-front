import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropriedadePage } from './propriedade.page';

describe('PropriedadePage', () => {
  let component: PropriedadePage;
  let fixture: ComponentFixture<PropriedadePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropriedadePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropriedadePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
