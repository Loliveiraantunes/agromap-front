import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Propriedade } from 'src/model/Propriedade';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Util } from 'src/util/Util';

@Component({
  selector: 'app-propriedade',
  templateUrl: './propriedade.page.html',
  styleUrls: ['./propriedade.page.scss'],
})
export class PropriedadePage implements OnInit {

  public propriedades: Propriedade[] = new Array();
  public propriedade: Propriedade = new Propriedade(null);


  constructor(
    public NavCtrl: NavController,
    public http: HttpClient,
    private alertController: AlertController,
    private route: ActivatedRoute) {

    this.route.params.subscribe(resp => {
      this.reload();
    });
  }

  private reload() {
    this.propriedades = new Array();
    this.http.get(Util.url + "/api/propriedade/user/"+Util.logged().id).subscribe((resp: Propriedade[]) => {
      resp.forEach((propriedade) => {
        this.propriedades.push(new Propriedade(propriedade));
      });
    });
  }

  visualizar(propriedade) {
    this.NavCtrl.navigateBack('/propriedade/visualizar/'+propriedade.id);
  }

  editar(id){
    this.NavCtrl.navigateBack('/propriedade/cadastrar/'+id); 
  }


  async deletar(id, nome) {
    const alert = await this.alertController.create({
      header: 'Excluir!',
      message: 'Deseja deletar "' + nome + '"? Após a Confirmação não é possível recuperar o registro.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Deletar',
          handler: () => {

            this.http.delete(Util.url + "/api/propriedade/remove/" + id).subscribe((resp) => {
              this.reload()
            });
          }
        }
      ]
    });
    await alert.present();
  }
  ngOnInit() {
  }

  cadastrar() {
    this.NavCtrl.navigateBack('/propriedade/cadastrar');
  }

}
