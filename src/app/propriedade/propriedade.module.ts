import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PropriedadePage } from './propriedade.page';
import {  HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: PropriedadePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PropriedadePage]
})
export class PropriedadePageModule {}
