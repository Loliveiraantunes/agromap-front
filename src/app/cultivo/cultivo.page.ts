import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cultura } from 'src/model/Cultura';
import { Cultivo } from 'src/model/Cultivo';
import { NavController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Util } from 'src/util/Util';
import { Propriedade } from 'src/model/Propriedade';

@Component({
  selector: 'app-cultivo',
  templateUrl: './cultivo.page.html',
  styleUrls: ['./cultivo.page.scss'],
})
export class CultivoPage implements OnInit {
  public form: FormGroup;
  public culturas: Cultura[] = new Array();
  public cultivo: Cultivo = new Cultivo(null);
  public id: any;
  public idPropriedade: any;
  public propriedade: Propriedade = new Propriedade(null);

  constructor(public NavCtrl: NavController,
    public formBuilder: FormBuilder,
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public route: Router) {

    this.form = this.formBuilder.group({
      cultura: ['', Validators.required],
      dataInicio: ['', Validators.required],
      dataFim: ['']
    });

  }
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.idPropriedade = this.activatedRoute.snapshot.paramMap.get('idPropriedade');

    if (this.id != null) {
      this.http.get(Util.url + "/api/cultivo/" + this.id).subscribe(resp => {
        console.log(resp)
        this.cultivo = new Cultivo(resp);
      });
    }
    this.http.get(Util.url + "/api/cultura/all").subscribe((resp: Cultura[]) => {
      resp.forEach((cultura) => {
        this.culturas.push(new Cultura(cultura));
      })
    });

    this.http.get(Util.url + "/api/propriedade/" + this.idPropriedade).subscribe((resp: Propriedade) => {
      this.propriedade = new Propriedade(resp);
    });

  }
  salvar() {
    this.cultivo.propriedade = this.propriedade;

    if (!this.form.valid) {
      this.invalid();
      return;
    }
    if (this.id == null) {
      this.cultivo.propriedade = this.propriedade;
      this.http.post(Util.url + "/api/cultivo/add", this.cultivo).subscribe(resp => {
        this.presentToast();
        this.NavCtrl.navigateRoot('propriedade/visualizar/' + this.idPropriedade);
      });
    } else {
      this.http.put(Util.url + "/api/cultivo/update", this.cultivo).subscribe(resp => {
        this.update();
        this.NavCtrl.navigateRoot('propriedade/visualizar/' + this.idPropriedade);
      });
    }

  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Cadastrada com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  async invalid() {
    const toast = await this.toastController.create({
      message: 'Verifique se os campos estão preenchidos corretamente',
      duration: 2000
    });
    toast.present();
  }
  async update() {
    const toast = await this.toastController.create({
      message: 'Atualizado com Sucesso',
      duration: 2000
    });
    toast.present();
  }
  back() {
    this.NavCtrl.back();
  }

  compareById(o1, o2) {
    return o1.id === o2.id
  }

  submit(){
    
  }
}
