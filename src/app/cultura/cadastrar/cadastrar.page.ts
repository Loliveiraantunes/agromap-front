import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Cultura } from 'src/model/Cultura';
import { Util } from 'src/util/Util';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.page.html',
  styleUrls: ['./cadastrar.page.scss'],
})
export class CadastrarPage implements OnInit {
  public form: FormGroup;

  public cultura:Cultura = new Cultura(null); 
  public id:any;
  constructor (public NavCtrl:NavController, 
    public formBuilder: FormBuilder, 
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public route:Router) {
      this.form = this.formBuilder.group({
        tipo:[''],
        nome:[''],
        descricao:['']
      });

      this.id = this.activatedRoute.snapshot.paramMap.get('id');

      if(this.id != null){
        this.http.get(Util.url+"/api/cultura/"+this.id).subscribe( resp =>{
          this.cultura = new Cultura(resp);
        });
      }
     }

     async invalid() {
      const toast = await this.toastController.create({
        message: 'Verifique se os campos estão preenchidos corretamente',
        duration: 2000
      });
      toast.present();
    }
  
    ngOnInit() {
  
    }
  
    salvar(){
    
      if(!this.form.valid){
        this.invalid();
        return;
      }
      if(this.id == null){
        this.cultura = new Cultura(this.form.value);
        this.http.post(Util.url+"/api/cultura/add",this.cultura).subscribe( (resp:Cultura) =>{
          this.presentToast() ;
          this.NavCtrl.navigateRoot('cultura/visualizar/'+resp.id);
        });
      }else{
        this.http.put(Util.url+"/api/cultura/update",this.cultura).subscribe( resp =>{
          this.update() ;
          this.route.navigate(['/cultura']);
        });
      }
     
    }
    async presentToast() {
      const toast = await this.toastController.create({
        message: 'Cultura cadastrada com Sucesso',
        duration: 2000
      });
      toast.present();
    }
  
    async update() {
      const toast = await this.toastController.create({
        message: 'Cultura atualizada com Sucesso',
        duration: 2000
      });
      toast.present();
    }
  back() {
    this.NavCtrl.back();
  }
  submit(){
    
  }
}
