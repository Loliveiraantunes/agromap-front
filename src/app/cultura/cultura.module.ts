import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CulturaPage } from './cultura.page';
import { ShowModalModule } from '../show-modal/show-modal.module';

const routes: Routes = [
  {
    path: '',
    component: CulturaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowModalModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CulturaPage]
})
export class CulturaPageModule {}
