import { Component, OnInit } from '@angular/core';
import { Util } from 'src/util/Util';
import { Cultura } from 'src/model/Cultura';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NavController, ToastController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Contamina } from 'src/model/Contamina';

@Component({
  selector: 'app-visualizar',
  templateUrl: './visualizar.page.html',
  styleUrls: ['./visualizar.page.scss'],
})
export class VisualizarPage implements OnInit {


  public cultura: Cultura = new Cultura(null);
  public contaminacao: Contamina[] = new Array();
  public id: any;

  constructor(public NavCtrl: NavController,
    public formBuilder: FormBuilder,
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    public http: HttpClient,
    public route: Router) {

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id != null) {
      this.http.get(Util.url + "/api/cultura/" + this.id).subscribe(resp => {
        this.cultura = new Cultura(resp);
      });
    }
  }

  async invalid() {
    const toast = await this.toastController.create({
      message: 'Verifique se os campos estão preenchidos corretamente',
      duration: 2000
    });
    toast.present();
  }

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.contaminacao = new Array();
    this.http.get(Util.url + "/api/contamina/cultura/" + this.id).subscribe((resp: Contamina[]) => {
      resp.forEach(contamina => {
        this.contaminacao.push(new Contamina(contamina));
      });
    });
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Cultura cadastrada com Sucesso',
      duration: 2000
    });
    toast.present();
  }

  async update() {
    const toast = await this.toastController.create({
      message: 'Cultura atualizada com Sucesso',
      duration: 2000
    });
    toast.present();
  }

  back() {
    this.NavCtrl.navigateBack('cultura');
  }

  addContamina() {
    this.NavCtrl.navigateBack('contamina/cadastrar/' + this.id);
  }

  editar(id) {
    this.NavCtrl.navigateBack('contamina/cadastrar/' + this.id + '/' + id);
  }

  async deletar(id, nome) {
    const alert = await this.alertController.create({
      header: 'Excluir!',
      message: 'Deseja deletar "' + nome + '"? Após a Confirmação não é possível recuperar o registro.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Deletar',
          handler: () => {

            this.http.delete(Util.url + "/api/contamina/remove/" + id).subscribe((resp) => {
              this.reload()
            });
          }
        }
      ]
    });
    await alert.present();
  }
}
