import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ModalController } from '@ionic/angular';
import { Cultura } from 'src/model/Cultura';
import { HttpClient } from '@angular/common/http';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ActivatedRoute } from '@angular/router';
import { ShowModalComponent } from '../show-modal/show-modal.component';
import { Util } from 'src/util/Util';

@Component({
  selector: 'app-cultura',
  templateUrl: './cultura.page.html',
  styleUrls: ['./cultura.page.scss'],
})
export class CulturaPage implements OnInit {

  culturas:Cultura[] = new Array();

  constructor(public NavCtrl: NavController,
    public http: HttpClient,
    private alertController: AlertController,
    private splashScreen: SplashScreen,
    public modalController: ModalController,
    private route: ActivatedRoute) {

      this.route.params.subscribe(resp => {
        this.reload();
      });


     }

  ngOnInit() {
  }

  visualizar(id) {
    this.NavCtrl.navigateForward('cultura/visualizar/'+id);
    }

  editar(id) {
    this.NavCtrl.navigateForward('cultura/cadastrar/' + id);
  }


  private reload() {
    this.culturas = new Array();
    this.http.get(Util.url + "/api/cultura/all").subscribe((resp: Cultura[]) => {
      resp.forEach((cultura) => {
        this.culturas.push(new Cultura(cultura));
      });
    });
  }

  async deletar(id, nome) {
    const alert = await this.alertController.create({
      header: 'Excluir!',
      message: 'Deseja deletar "' + nome + '"? Após a Confirmação não é possível recuperar o registro.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Deletar',
          handler: () => {

            this.http.delete(Util.url + "/api/cultura/remove/" + id).subscribe((resp) => {
              this.reload()
            });
          }
        }
      ]
    });
    await alert.present();
  }
  cadastrar(){
    this.NavCtrl.navigateForward('cultura/cadastrar');
  }


}
