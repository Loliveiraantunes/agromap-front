import { User } from 'src/model/User';
import { AppComponent } from 'src/app/app.component';

export class Util {
//172.28.3.68
//192.168.0.15
  public static get url():string { return "http://localhost:8080"}
  

  public static logged():User{
    return new User( JSON.parse(localStorage.getItem('user_id')));
  }

  public static loggin(user:any){
    localStorage.clear();
    localStorage.setItem("user_id",JSON.stringify(user));
    localStorage.setItem("role",user.role);


    let img  = Math.floor(Math.random() * 5) + 1 ; 
    localStorage.setItem("img",img+".png");
  }


}