var Cultivo = /** @class */ (function () {
    function Cultivo(Obj) {
        if (Obj != null) {
            this.id = Obj.id;
            this.dataInicio = Obj.dataInicio;
            this.dataFim = Obj.dataFim;
            this.cultura = Obj.cultura;
            this.propriedade = Obj.propriedade;
        }
    }
    return Cultivo;
}());
export { Cultivo };
//# sourceMappingURL=Cultivo.js.map