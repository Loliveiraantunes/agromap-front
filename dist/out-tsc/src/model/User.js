var User = /** @class */ (function () {
    function User(Obj) {
        if (Obj != null) {
            this.id = Obj.id;
            this.email = Obj.email;
            this.login = Obj.login;
            this.password = Obj.password;
            this.role = Obj.role;
            this.pessoa = Obj.pessoa;
        }
    }
    return User;
}());
export { User };
//# sourceMappingURL=User.js.map