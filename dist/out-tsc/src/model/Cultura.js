var Cultura = /** @class */ (function () {
    function Cultura(Obj) {
        if (Obj != null) {
            this.id = Obj.id;
            this.nome = Obj.nome;
            this.tipo = Obj.tipo;
            this.descricao = Obj.descricao;
        }
    }
    return Cultura;
}());
export { Cultura };
//# sourceMappingURL=Cultura.js.map