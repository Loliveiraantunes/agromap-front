var Ticket = /** @class */ (function () {
    function Ticket(Obj) {
        if (Obj != null) {
            this.id = Obj.id;
            this.status = Obj.status;
            this.descricao = Obj.descricao;
            this.dataCriacao = Obj.dataCriacao;
            this.dataFechamento = Obj.dataFechamento;
            this.cultivo = Obj.cultivo;
            this.contamina = Obj.contamina;
            this.tratamento = Obj.tratamento;
        }
    }
    return Ticket;
}());
export { Ticket };
//# sourceMappingURL=Ticket.js.map