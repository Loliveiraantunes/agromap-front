var Contamina = /** @class */ (function () {
    function Contamina(Obj) {
        if (Obj != null) {
            this.id = Obj.id;
            this.doenca = Obj.doenca;
            this.cultura = Obj.cultura;
        }
    }
    return Contamina;
}());
export { Contamina };
//# sourceMappingURL=Contamina.js.map