var Doenca = /** @class */ (function () {
    function Doenca(Obj) {
        if (Obj != null) {
            this.id = Obj.id;
            this.nome = Obj.nome;
            this.causador = Obj.causador;
            this.descricao = Obj.descricao;
            this.foto = Obj.foto;
        }
    }
    return Doenca;
}());
export { Doenca };
//# sourceMappingURL=Doenca.js.map