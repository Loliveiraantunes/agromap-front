var Propriedade = /** @class */ (function () {
    function Propriedade(Obj) {
        if (Obj != null) {
            if (Obj.id != null) {
                this.id = Obj.id;
            }
            this.logradouro = Obj.logradouro;
            this.cep = Obj.cep;
            this.cidade = Obj.cidade;
            this.bairro = Obj.bairro;
            this.uf = Obj.uf;
            this.complemento = Obj.complemento;
            this.latitude = Obj.latitude;
            this.longitude = Obj.longitude;
            this.documento = Obj.documento;
            this.kmQuadrados = Obj.kmQuadrados;
            this.cadastro = Obj.cadastro;
            this.user = Obj.user;
        }
    }
    return Propriedade;
}());
export { Propriedade };
//# sourceMappingURL=Propriedade.js.map