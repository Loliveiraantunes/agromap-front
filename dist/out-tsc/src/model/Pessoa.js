var Pessoa = /** @class */ (function () {
    function Pessoa(Obj) {
        if (Obj != null) {
            this.id = Obj.id;
            this.nome = Obj.nome;
            this.sobrenome = Obj.sobrenome;
            this.sexo = Obj.sexo;
            this.nascimento = Obj.nascimento;
            this.cpf = Obj.cpf;
            this.user = Obj.user;
        }
    }
    return Pessoa;
}());
export { Pessoa };
//# sourceMappingURL=Pessoa.js.map