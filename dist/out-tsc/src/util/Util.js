import { User } from 'src/model/User';
var Util = /** @class */ (function () {
    function Util() {
    }
    Object.defineProperty(Util, "url", {
        //172.28.3.68
        //192.168.0.15
        get: function () { return "http://192.168.0.15:8080"; },
        enumerable: true,
        configurable: true
    });
    Util.logged = function () {
        return new User(JSON.parse(localStorage.getItem('user_id')));
    };
    Util.loggin = function (user) {
        localStorage.clear();
        localStorage.setItem("user_id", JSON.stringify(user));
        var img = Math.floor(Math.random() * 5) + 1;
        localStorage.setItem("img", img + ".png");
    };
    return Util;
}());
export { Util };
//# sourceMappingURL=Util.js.map