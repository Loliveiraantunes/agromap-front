import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { CadastrarModalComponent } from './cadastrar-modal.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
var CadastrarModalModule = /** @class */ (function () {
    function CadastrarModalModule() {
    }
    CadastrarModalModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                IonicModule,
                ReactiveFormsModule,
                HttpClientModule,
                FormsModule
            ],
            declarations: [
                CadastrarModalComponent
            ],
            entryComponents: [
                CadastrarModalComponent
            ],
            exports: [
                CadastrarModalComponent
            ]
        })
    ], CadastrarModalModule);
    return CadastrarModalModule;
}());
export { CadastrarModalModule };
//# sourceMappingURL=cadastrar-modal.module.js.map