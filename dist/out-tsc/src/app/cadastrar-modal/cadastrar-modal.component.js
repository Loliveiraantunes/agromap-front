import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuController, ModalController, ToastController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/model/User';
import { Pessoa } from 'src/model/Pessoa';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
var CadastrarModalComponent = /** @class */ (function () {
    function CadastrarModalComponent(menuCtrl, formBuilder, http, modal, toastController) {
        this.menuCtrl = menuCtrl;
        this.formBuilder = formBuilder;
        this.http = http;
        this.modal = modal;
        this.toastController = toastController;
        this.form = this.formBuilder.group({
            login: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', Validators.required],
            copassword: ['', Validators.required],
            nome: ['', Validators.required],
            sobrenome: ['', Validators.required],
            cpf: ['', Validators.required],
            sexo: ['', Validators.required],
            nascimento: ['', Validators.required],
        });
    }
    CadastrarModalComponent.prototype.submit = function () {
        this.cadastrarUser();
    };
    CadastrarModalComponent.prototype.cadastrarUser = function () {
        var _this = this;
        this.user = new User(this.form.value);
        this.pessoa = new Pessoa(this.form.value);
        if (!this.form.valid) {
            this.invalido();
            return;
        }
        if (this.form.value.password == this.form.value.copassword) {
            this.http.post(Util.url + "/api/user/add", this.user).subscribe(function (resp) {
                var userJS = new User(resp);
                _this.pessoa.user = userJS;
                _this.cadastrarPessoa(_this.pessoa);
            });
        }
        else {
            this.presentToastPassword();
            return;
        }
    };
    CadastrarModalComponent.prototype.cadastrarPessoa = function (pessoa) {
        var _this = this;
        this.http.post(Util.url + "/api/pessoa/add", pessoa).subscribe(function (resp) {
            console.log(resp);
            _this.dismiss();
            _this.presentToast();
        });
    };
    CadastrarModalComponent.prototype.presentToastPassword = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'As senhas não coincidem.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarModalComponent.prototype.invalido = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Verifique se todos os campos foram preenchidos corretamente.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarModalComponent.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Sua conta foi criada com sucesso.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarModalComponent.prototype.dismiss = function () {
        this.modal.dismiss();
    };
    CadastrarModalComponent.prototype.ngOnInit = function () { };
    CadastrarModalComponent = tslib_1.__decorate([
        Component({
            selector: 'app-cadastrar-modal',
            templateUrl: './cadastrar-modal.component.html',
            styleUrls: ['./cadastrar-modal.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuController,
            FormBuilder,
            HttpClient,
            ModalController,
            ToastController])
    ], CadastrarModalComponent);
    return CadastrarModalComponent;
}());
export { CadastrarModalComponent };
//# sourceMappingURL=cadastrar-modal.component.js.map