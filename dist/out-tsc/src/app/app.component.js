import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { User } from 'src/model/User';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.appPages = [
            {
                title: 'Home',
                url: '/home',
                icon: 'home'
            },
            {
                title: 'Propriedade',
                url: '/propriedade',
                icon: 'pin'
            },
            {
                title: 'Cultura',
                url: '/cultura',
                icon: 'ios-leaf'
            },
            {
                title: 'Enfermidades',
                url: '/doenca',
                icon: 'ios-bug'
            },
            {
                title: 'Contamina',
                url: '/contamina',
                icon: 'ios-archive'
            }
        ];
        this.initializeApp();
    }
    AppComponent.prototype.ngAfterContentInit = function () {
        if (localStorage.getItem('user_id') != null) {
            this.user = new User(JSON.parse(localStorage.getItem('user_id')));
            this.nome = this.user.pessoa.nome;
            this.sobrenome = this.user.pessoa.sobrenome;
        }
        if (localStorage.getItem('img') != null) {
            this.img = "assets/icon/" + localStorage.getItem('img');
        }
    };
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SplashScreen,
            StatusBar])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map