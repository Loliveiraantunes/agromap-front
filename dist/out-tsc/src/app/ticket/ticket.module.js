import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TicketPage } from './ticket.page';
import { HttpClientModule } from '@angular/common/http';
var routes = [
    {
        path: '',
        component: TicketPage
    }
];
var TicketPageModule = /** @class */ (function () {
    function TicketPageModule() {
    }
    TicketPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TicketPage]
        })
    ], TicketPageModule);
    return TicketPageModule;
}());
export { TicketPageModule };
//# sourceMappingURL=ticket.module.js.map