import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Ticket } from 'src/model/Ticket';
import { Cultura } from 'src/model/Cultura';
import { Util } from 'src/util/Util';
import { Cultivo } from 'src/model/Cultivo';
import { Contamina } from 'src/model/Contamina';
var TicketPage = /** @class */ (function () {
    function TicketPage(NavCtrl, formBuilder, toastController, activatedRoute, http, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.formBuilder = formBuilder;
        this.toastController = toastController;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.route = route;
        this.ticket = new Ticket(null);
        this.culturas = new Array();
        this.contaminacao = new Array();
        this.doencas = new Array();
        this.cultura = new Cultura(null);
        this.cultivo = new Cultivo(null);
        this.id = this.activatedRoute.snapshot.paramMap.get('idCultivo');
        if (this.id != null) {
            this.http.get(Util.url + "/api/cultivo/" + this.id).subscribe(function (resp) {
                console.log(resp);
                _this.cultivo = new Cultivo(resp);
                _this.http.get(Util.url + "/api/contamina/cultura/" + _this.cultivo.cultura.id).subscribe(function (resp) {
                    console.log(resp);
                    resp.forEach(function (conta) {
                        _this.contaminacao.push(new Contamina(conta));
                    });
                });
            });
        }
        this.form = this.formBuilder.group({
            descricao: ['', Validators.required],
            cultura: ['', Validators.required],
            contamina: ['', Validators.required],
        });
    }
    TicketPage.prototype.compareById = function (o1, o2) {
        return o1.id === o2.id;
    };
    TicketPage.prototype.ngOnInit = function () {
        var _this = this;
        this.culturas = new Array();
        this.http.get(Util.url + "/api/cultura/all").subscribe(function (resp) {
            resp.forEach(function (cultura) {
                _this.culturas.push(new Cultura(cultura));
            });
        });
    };
    TicketPage.prototype.salvar = function () {
        var _this = this;
        if (!this.form.valid) {
            this.invalid();
            return;
        }
        if (this.idTicket == null) {
            this.ticket.dataCriacao = new Date();
            this.ticket.status = 1;
            this.ticket.cultivo = this.cultivo;
            this.ticket.contamina = this.form.value.contamina;
            this.ticket.descricao = this.form.value.descricao;
            this.http.post(Util.url + "/api/ticket/add", this.ticket).subscribe(function (resp) {
                _this.presentToast();
                _this.NavCtrl.back();
            });
        }
        else {
            this.http.put(Util.url + "/api/ticket/update/" + this.idTicket, this.ticket).subscribe(function (resp) {
                _this.update();
                _this.NavCtrl.back();
            });
        }
    };
    TicketPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Cadastrado com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    TicketPage.prototype.update = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Atualizado com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    TicketPage.prototype.invalid = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Verifique se os campos estão preenchidos corretamente',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    TicketPage.prototype.back = function () {
        this.NavCtrl.back();
    };
    TicketPage = tslib_1.__decorate([
        Component({
            selector: 'app-ticket',
            templateUrl: './ticket.page.html',
            styleUrls: ['./ticket.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            FormBuilder,
            ToastController,
            ActivatedRoute,
            HttpClient,
            Router])
    ], TicketPage);
    return TicketPage;
}());
export { TicketPage };
//# sourceMappingURL=ticket.page.js.map