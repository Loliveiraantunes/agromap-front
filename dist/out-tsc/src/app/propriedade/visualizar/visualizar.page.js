import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, ToastController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { Propriedade } from 'src/model/Propriedade';
import { Cultivo } from 'src/model/Cultivo';
var VisualizarPage = /** @class */ (function () {
    function VisualizarPage(NavCtrl, toastController, activatedRoute, alertController, http, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.toastController = toastController;
        this.activatedRoute = activatedRoute;
        this.alertController = alertController;
        this.http = http;
        this.route = route;
        this.propriedade = new Propriedade(null);
        this.cultivos = new Array();
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        if (this.id != null) {
            this.http.get(Util.url + "/api/propriedade/" + this.id).subscribe(function (resp) {
                _this.propriedade = new Propriedade(resp);
                _this.map.setCenter(new google.maps.LatLng(_this.propriedade.latitude, _this.propriedade.longitude));
                _this.map.setZoom(17);
                _this.marker.setPosition(new google.maps.LatLng(_this.propriedade.latitude, _this.propriedade.longitude));
                _this.marker.setVisible(true);
                _this.marker.setMap(_this.map);
            });
        }
        this.route.params.subscribe(function (resp) {
            _this.reload();
        });
    }
    VisualizarPage.prototype.ngAfterContentInit = function () {
        this.loadMap();
    };
    VisualizarPage.prototype.ngOnInit = function () {
    };
    VisualizarPage.prototype.loadMap = function () {
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: -27.5967945, lng: -48.5538321 },
            zoom: 15,
            draggable: false,
            disableDefaultUI: true
        });
        this.marker = new google.maps.Marker({
            map: this.map,
            draggable: false
        });
    };
    VisualizarPage.prototype.adicionarCultivo = function () {
        this.NavCtrl.navigateForward('cultivo/' + this.id);
    };
    VisualizarPage.prototype.back = function () {
        this.NavCtrl.back();
    };
    VisualizarPage.prototype.ticket = function (idCultivo) {
        this.NavCtrl.navigateForward('ticket/' + idCultivo);
    };
    VisualizarPage.prototype.editar = function (idCultivo) {
        this.NavCtrl.navigateForward('cultivo/' + this.id + "/" + idCultivo);
    };
    VisualizarPage.prototype.deletar = function (id, cultivo) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Excluir!',
                            message: 'Deseja deletar um Cultivo de "' + cultivo.cultura.nome + '"? Após a Confirmação não é possível recuperar o registro.',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                    }
                                }, {
                                    text: 'Deletar',
                                    handler: function () {
                                        _this.http.delete(Util.url + "/api/cultivo/remove/" + id).subscribe(function (resp) {
                                            _this.reload();
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    VisualizarPage.prototype.reload = function () {
        var _this = this;
        this.cultivos = new Array();
        this.http.get(Util.url + "/api/cultivo/propriedade/" + this.id).subscribe(function (resp) {
            resp.forEach(function (cultivo) {
                _this.cultivos.push(new Cultivo(cultivo));
            });
        });
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], VisualizarPage.prototype, "mapElement", void 0);
    VisualizarPage = tslib_1.__decorate([
        Component({
            selector: 'app-visualizar',
            templateUrl: './visualizar.page.html',
            styleUrls: ['./visualizar.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            ToastController,
            ActivatedRoute,
            AlertController,
            HttpClient,
            ActivatedRoute])
    ], VisualizarPage);
    return VisualizarPage;
}());
export { VisualizarPage };
//# sourceMappingURL=visualizar.page.js.map