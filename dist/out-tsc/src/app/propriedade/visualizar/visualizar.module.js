import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { VisualizarPage } from './visualizar.page';
import { HttpClientModule } from '@angular/common/http';
var routes = [
    {
        path: '',
        component: VisualizarPage
    }
];
var VisualizarPageModule = /** @class */ (function () {
    function VisualizarPageModule() {
    }
    VisualizarPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                HttpClientModule,
                RouterModule.forChild(routes)
            ],
            declarations: [VisualizarPage]
        })
    ], VisualizarPageModule);
    return VisualizarPageModule;
}());
export { VisualizarPageModule };
//# sourceMappingURL=visualizar.module.js.map