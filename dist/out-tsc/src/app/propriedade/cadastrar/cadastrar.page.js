import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Propriedade } from 'src/model/Propriedade';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
var CadastrarPage = /** @class */ (function () {
    function CadastrarPage(NavCtrl, formBuilder, toastController, activatedRoute, http, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.formBuilder = formBuilder;
        this.toastController = toastController;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.route = route;
        this.propriedade = new Propriedade(null);
        this.form = this.formBuilder.group({
            cep: ['', Validators.required],
            logradouro: ['', Validators.required],
            uf: [''],
            cidade: [''],
            bairro: [''],
            latitude: [''],
            longitude: [''],
            cadastro: [''],
            complemento: [''],
            documento: [''],
            kmQuadrados: ['', Validators.required]
        });
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        if (this.id != null) {
            this.http.get(Util.url + "/api/propriedade/" + this.id).subscribe(function (resp) {
                _this.propriedade = new Propriedade(resp);
                _this.map.setCenter(new google.maps.LatLng(_this.propriedade.latitude, _this.propriedade.longitude));
                _this.map.setZoom(17);
                _this.marker.setPosition(new google.maps.LatLng(_this.propriedade.latitude, _this.propriedade.longitude));
                _this.marker.setVisible(true);
                _this.marker.setMap(_this.map);
            });
        }
    }
    CadastrarPage.prototype.ngOnInit = function () {
    };
    CadastrarPage.prototype.salvar = function () {
        var _this = this;
        this.propriedade.kmQuadrados = this.form.value.kmQuadrados;
        this.propriedade.user = Util.logged();
        if (!this.form.valid) {
            this.invalid();
            return;
        }
        if (this.id == null) {
            this.propriedade.cadastro = new Date();
            this.http.post(Util.url + "/api/propriedade/add", this.propriedade).subscribe(function (resp) {
                _this.presentToast();
                _this.NavCtrl.navigateRoot('propriedade/visualizar/' + resp.id);
            });
        }
        else {
            this.http.put(Util.url + "/api/propriedade/update/" + this.id, this.propriedade).subscribe(function (resp) {
                _this.update();
                _this.route.navigate(['/propriedade']);
            });
        }
    };
    CadastrarPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Propriedade Cadastrada com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage.prototype.update = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Propriedade atualizada com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage.prototype.invalid = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Verifique se os campos estão preenchidos corretamente',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage.prototype.back = function () {
        this.NavCtrl.back();
    };
    CadastrarPage.prototype.ngAfterContentInit = function () {
        this.loadMap();
    };
    CadastrarPage.prototype.loadMap = function () {
        var _this = this;
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: -27.5967945, lng: -48.5538321 },
            zoom: 15,
            disableDefaultUI: true
        });
        this.geocoder = new google.maps.Geocoder;
        this.autocomplete = new google.maps.places.Autocomplete(this.autocompleteElement.nativeElement);
        this.autocomplete.bindTo('bounds', this.map);
        this.autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
        this.marker = new google.maps.Marker({
            map: this.map,
            draggable: true
        });
        this.autocomplete.addListener('place_changed', function (resp) {
            _this.propriedade.logradouro = "...";
            _this.propriedade.uf = "...";
            _this.propriedade.cidade = "...";
            _this.propriedade.bairro = "...";
            _this.propriedade.cep = "...";
            // this.infowindow.close();
            _this.marker.setVisible(false);
            var place = _this.autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            if (place.geometry.viewport) {
                _this.map.fitBounds(place.geometry.viewport);
            }
            else {
                _this.map.setCenter(place.geometry.location);
                _this.map.setZoom(17);
            }
            _this.marker.setPosition(place.geometry.location);
            _this.marker.setVisible(true);
            var address = '';
            if (place.address_components) {
                _this.geocodeLatLng(_this.geocoder, _this.map, _this.marker.getPosition().lat() + "," + _this.marker.getPosition().lng());
            }
        });
        google.maps.event.addListener(this.marker, "dragend", function (event) {
            _this.propriedade.latitude = event.latLng.lat();
            _this.propriedade.longitude = event.latLng.lng();
            _this.propriedade.user = Util.logged();
            _this.propriedade.cadastro = new Date();
            _this.geocodeLatLng(_this.geocoder, _this.map, (event.latLng.lat() + "," + event.latLng.lng()));
        });
    };
    CadastrarPage.prototype.geocodeLatLng = function (geocoder, map, latLngCoords) {
        var _this = this;
        var latlngStr = latLngCoords.split(',', 2);
        var latlng = { lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1]) };
        geocoder.geocode({ 'location': latlng }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    // console.log(results);
                    if (results[0].address_components.length == 7) {
                        _this.propriedade.logradouro = results[0].address_components[1].long_name;
                        _this.propriedade.uf = results[0].address_components[4].short_name;
                        _this.propriedade.cidade = results[0].address_components[3].long_name;
                        _this.propriedade.bairro = results[0].address_components[2].long_name;
                        _this.propriedade.cep = results[0].address_components[6].long_name;
                        _this.propriedade.latitude = latlngStr[0];
                        _this.propriedade.longitude = latlngStr[1];
                    }
                    else if (results[0].address_components.length == 6) {
                        _this.propriedade.logradouro = results[0].address_components[1].long_name;
                        _this.propriedade.uf = results[0].address_components[3].short_name;
                        _this.propriedade.cidade = results[0].address_components[2].long_name;
                        _this.propriedade.bairro = results[0].address_components[2].long_name;
                        _this.propriedade.cep = results[0].address_components[5].long_name;
                        _this.propriedade.latitude = latlngStr[0];
                        _this.propriedade.longitude = latlngStr[1];
                    }
                    else if (results[0].address_components.length == 5) {
                        _this.propriedade.logradouro = results[0].address_components[0].long_name;
                        _this.propriedade.uf = results[0].address_components[3].short_name;
                        _this.propriedade.cidade = results[0].address_components[2].long_name;
                        _this.propriedade.bairro = results[0].address_components[0].long_name;
                        _this.propriedade.cep = results[0].address_components[4].long_name;
                        _this.propriedade.latitude = latlngStr[0];
                        _this.propriedade.longitude = latlngStr[1];
                    }
                }
                else {
                    window.alert('No results found');
                }
            }
            else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], CadastrarPage.prototype, "mapElement", void 0);
    tslib_1.__decorate([
        ViewChild('autocomplete'),
        tslib_1.__metadata("design:type", ElementRef)
    ], CadastrarPage.prototype, "autocompleteElement", void 0);
    tslib_1.__decorate([
        ViewChild('infoWindow'),
        tslib_1.__metadata("design:type", ElementRef)
    ], CadastrarPage.prototype, "infoWindowElement", void 0);
    CadastrarPage = tslib_1.__decorate([
        Component({
            selector: 'app-cadastrar',
            templateUrl: './cadastrar.page.html',
            styleUrls: ['./cadastrar.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            FormBuilder,
            ToastController,
            ActivatedRoute,
            HttpClient,
            Router])
    ], CadastrarPage);
    return CadastrarPage;
}());
export { CadastrarPage };
//# sourceMappingURL=cadastrar.page.js.map