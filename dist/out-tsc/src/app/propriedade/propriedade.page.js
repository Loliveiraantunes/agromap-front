import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Propriedade } from 'src/model/Propriedade';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Util } from 'src/util/Util';
var PropriedadePage = /** @class */ (function () {
    function PropriedadePage(NavCtrl, http, alertController, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.http = http;
        this.alertController = alertController;
        this.route = route;
        this.propriedades = new Array();
        this.propriedade = new Propriedade(null);
        this.route.params.subscribe(function (resp) {
            _this.reload();
        });
    }
    PropriedadePage.prototype.reload = function () {
        var _this = this;
        this.propriedades = new Array();
        this.http.get(Util.url + "/api/propriedade/user/" + Util.logged().id).subscribe(function (resp) {
            resp.forEach(function (propriedade) {
                _this.propriedades.push(new Propriedade(propriedade));
            });
        });
    };
    PropriedadePage.prototype.visualizar = function (propriedade) {
        this.NavCtrl.navigateBack('/propriedade/visualizar/' + propriedade.id);
    };
    PropriedadePage.prototype.editar = function (id) {
        this.NavCtrl.navigateBack('/propriedade/cadastrar/' + id);
    };
    PropriedadePage.prototype.deletar = function (id, nome) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Excluir!',
                            message: 'Deseja deletar "' + nome + '"? Após a Confirmação não é possível recuperar o registro.',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                    }
                                }, {
                                    text: 'Deletar',
                                    handler: function () {
                                        _this.http.delete(Util.url + "/api/propriedade/remove/" + id).subscribe(function (resp) {
                                            _this.reload();
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PropriedadePage.prototype.ngOnInit = function () {
    };
    PropriedadePage.prototype.cadastrar = function () {
        this.NavCtrl.navigateBack('/propriedade/cadastrar');
    };
    PropriedadePage = tslib_1.__decorate([
        Component({
            selector: 'app-propriedade',
            templateUrl: './propriedade.page.html',
            styleUrls: ['./propriedade.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            HttpClient,
            AlertController,
            ActivatedRoute])
    ], PropriedadePage);
    return PropriedadePage;
}());
export { PropriedadePage };
//# sourceMappingURL=propriedade.page.js.map