import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PropriedadePage } from './propriedade.page';
import { HttpClientModule } from '@angular/common/http';
var routes = [
    {
        path: '',
        component: PropriedadePage
    }
];
var PropriedadePageModule = /** @class */ (function () {
    function PropriedadePageModule() {
    }
    PropriedadePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PropriedadePage]
        })
    ], PropriedadePageModule);
    return PropriedadePageModule;
}());
export { PropriedadePageModule };
//# sourceMappingURL=propriedade.module.js.map