import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LogoutPage } from './logout.page';
var routes = [
    {
        path: '',
        component: LogoutPage
    }
];
var LogoutPageModule = /** @class */ (function () {
    function LogoutPageModule() {
    }
    LogoutPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [LogoutPage]
        })
    ], LogoutPageModule);
    return LogoutPageModule;
}());
export { LogoutPageModule };
//# sourceMappingURL=logout.module.js.map