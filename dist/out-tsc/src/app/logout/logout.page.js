import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
var LogoutPage = /** @class */ (function () {
    function LogoutPage(NavCtrl, menu) {
        this.NavCtrl = NavCtrl;
        this.menu = menu;
    }
    LogoutPage.prototype.ngOnInit = function () {
        localStorage.clear();
        this.NavCtrl.navigateRoot('');
        this.menu.enable(false);
    };
    LogoutPage = tslib_1.__decorate([
        Component({
            selector: 'app-logout',
            templateUrl: './logout.page.html',
            styleUrls: ['./logout.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, MenuController])
    ], LogoutPage);
    return LogoutPage;
}());
export { LogoutPage };
//# sourceMappingURL=logout.page.js.map