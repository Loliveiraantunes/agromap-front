import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ContaminaPage } from './contamina.page';
import { HttpClientModule } from '@angular/common/http';
var routes = [
    {
        path: '',
        component: ContaminaPage
    }
];
var ContaminaPageModule = /** @class */ (function () {
    function ContaminaPageModule() {
    }
    ContaminaPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ContaminaPage]
        })
    ], ContaminaPageModule);
    return ContaminaPageModule;
}());
export { ContaminaPageModule };
//# sourceMappingURL=contamina.module.js.map