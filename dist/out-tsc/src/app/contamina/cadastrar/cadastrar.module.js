import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CadastrarPage } from './cadastrar.page';
import { HttpClientModule } from '@angular/common/http';
var routes = [
    {
        path: '',
        component: CadastrarPage
    }
];
var CadastrarPageModule = /** @class */ (function () {
    function CadastrarPageModule() {
    }
    CadastrarPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CadastrarPage]
        })
    ], CadastrarPageModule);
    return CadastrarPageModule;
}());
export { CadastrarPageModule };
//# sourceMappingURL=cadastrar.module.js.map