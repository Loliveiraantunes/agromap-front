import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Contamina } from 'src/model/Contamina';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { Cultura } from 'src/model/Cultura';
import { Doenca } from 'src/model/Doenca';
var CadastrarPage = /** @class */ (function () {
    function CadastrarPage(NavCtrl, formBuilder, toastController, activatedRoute, http, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.formBuilder = formBuilder;
        this.toastController = toastController;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.route = route;
        this.culturas = new Array();
        this.doencas = new Array();
        this.doenca = new Doenca(null);
        this.cultura = new Cultura(null);
        this.form = this.formBuilder.group({
            cultura: ['', Validators.required],
            doenca: ['', Validators.required]
        });
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        if (this.id != null) {
            this.http.get(Util.url + "/api/contamina/" + this.id).subscribe(function (resp) {
                console.log(resp);
                _this.contamina = new Contamina(resp);
                _this.doenca = new Doenca(_this.contamina.doenca);
                _this.cultura = new Cultura(_this.contamina.cultura);
            });
        }
    }
    CadastrarPage.prototype.ngOnInit = function () {
        var _this = this;
        this.culturas = new Array();
        this.http.get(Util.url + "/api/cultura/all").subscribe(function (resp) {
            resp.forEach(function (cultura) {
                _this.culturas.push(new Cultura(cultura));
            });
        });
        this.doencas = new Array();
        this.http.get(Util.url + "/api/doenca/all").subscribe(function (resp) {
            resp.forEach(function (doenca) {
                _this.doencas.push(new Doenca(doenca));
            });
        });
    };
    CadastrarPage.prototype.salvar = function () {
        var _this = this;
        if (!this.form.valid) {
            this.invalid();
            return;
        }
        if (this.id == null) {
            this.contamina = new Contamina(this.form.value);
            this.http.post(Util.url + "/api/contamina/add", this.contamina).subscribe(function (resp) {
                _this.presentToast();
                _this.NavCtrl.navigateRoot('contamina');
            });
        }
        else {
            this.contamina.cultura = this.form.value.cultura;
            this.contamina.doenca = this.form.value.doenca;
            this.http.put(Util.url + "/api/contamina/update", this.contamina).subscribe(function (resp) {
                _this.update();
                _this.route.navigate(['/contamina']);
            });
        }
    };
    CadastrarPage.prototype.compareById = function (o1, o2) {
        return o1.id === o2.id;
    };
    CadastrarPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Registro cadastrado com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage.prototype.update = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Registro atualizado com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage.prototype.back = function () {
        this.NavCtrl.back();
    };
    CadastrarPage.prototype.invalid = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Verifique se os campos estão preenchidos corretamente',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage = tslib_1.__decorate([
        Component({
            selector: 'app-cadastrar',
            templateUrl: './cadastrar.page.html',
            styleUrls: ['./cadastrar.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            FormBuilder,
            ToastController,
            ActivatedRoute,
            HttpClient,
            Router])
    ], CadastrarPage);
    return CadastrarPage;
}());
export { CadastrarPage };
//# sourceMappingURL=cadastrar.page.js.map