import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Contamina } from 'src/model/Contamina';
import { NavController, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ActivatedRoute } from '@angular/router';
import { Util } from 'src/util/Util';
var ContaminaPage = /** @class */ (function () {
    function ContaminaPage(NavCtrl, http, alertController, splashScreen, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.http = http;
        this.alertController = alertController;
        this.splashScreen = splashScreen;
        this.route = route;
        this.contaminacao = new Array();
        this.route.params.subscribe(function (resp) {
            _this.reload();
        });
    }
    ContaminaPage.prototype.ngOnInit = function () {
    };
    ContaminaPage.prototype.editar = function (id) {
        this.NavCtrl.navigateForward('contamina/cadastrar/' + id);
    };
    ContaminaPage.prototype.reload = function () {
        var _this = this;
        this.contaminacao = new Array();
        this.http.get(Util.url + "/api/contamina/all").subscribe(function (resp) {
            resp.forEach(function (contamina) {
                _this.contaminacao.push(new Contamina(contamina));
            });
        });
    };
    ContaminaPage.prototype.deletar = function (id, contaminacao) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Excluir!',
                            message: 'Deseja deletar "' + contaminacao.cultura.nome + ' e ' + contaminacao.doenca.nome + '"? Após a Confirmação não é possível recuperar o registro.',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                    }
                                }, {
                                    text: 'Deletar',
                                    handler: function () {
                                        _this.http.delete(Util.url + "/api/contamina/remove/" + id).subscribe(function (resp) {
                                            _this.reload();
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ContaminaPage.prototype.cadastrar = function () {
        this.NavCtrl.navigateForward('contamina/cadastrar');
    };
    ContaminaPage = tslib_1.__decorate([
        Component({
            selector: 'app-contamina',
            templateUrl: './contamina.page.html',
            styleUrls: ['./contamina.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            HttpClient,
            AlertController,
            SplashScreen,
            ActivatedRoute])
    ], ContaminaPage);
    return ContaminaPage;
}());
export { ContaminaPage };
//# sourceMappingURL=contamina.page.js.map