import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
var routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: './home/home.module#HomePageModule'
    },
    {
        path: 'list',
        loadChildren: './list/list.module#ListPageModule'
    },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'propriedade', loadChildren: './propriedade/propriedade.module#PropriedadePageModule' },
    { path: 'propriedade/cadastrar', loadChildren: './propriedade/cadastrar/cadastrar.module#CadastrarPageModule' },
    { path: 'propriedade/cadastrar/:id', loadChildren: './propriedade/cadastrar/cadastrar.module#CadastrarPageModule' },
    { path: 'propriedade/visualizar/:id', loadChildren: './propriedade/visualizar/visualizar.module#VisualizarPageModule' },
    { path: 'cultura', loadChildren: './cultura/cultura.module#CulturaPageModule' },
    { path: 'cultura/cadastrar', loadChildren: './cultura/cadastrar/cadastrar.module#CadastrarPageModule' },
    { path: 'cultura/cadastrar/:id', loadChildren: './cultura/cadastrar/cadastrar.module#CadastrarPageModule' },
    { path: 'doenca', loadChildren: './doenca/doenca.module#DoencaPageModule' },
    { path: 'doenca/cadastrar', loadChildren: './doenca/cadastrar/cadastrar.module#CadastrarPageModule' },
    { path: 'doenca/cadastrar/:id', loadChildren: './doenca/cadastrar/cadastrar.module#CadastrarPageModule' },
    { path: 'contamina', loadChildren: './contamina/contamina.module#ContaminaPageModule' },
    { path: 'contamina/cadastrar', loadChildren: './contamina/cadastrar/cadastrar.module#CadastrarPageModule' },
    { path: 'contamina/cadastrar/:id', loadChildren: './contamina/cadastrar/cadastrar.module#CadastrarPageModule' },
    { path: 'cultivo/:idPropriedade', loadChildren: './cultivo/cultivo.module#CultivoPageModule' },
    { path: 'cultivo/:idPropriedade/:id', loadChildren: './cultivo/cultivo.module#CultivoPageModule' },
    { path: 'ticket/:idCultivo', loadChildren: './ticket/ticket.module#TicketPageModule' },
    { path: 'logout', loadChildren: './logout/logout.module#LogoutPageModule' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
            ],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map