import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
var HomePage = /** @class */ (function () {
    function HomePage() {
    }
    HomePage.prototype.ionViewDidLoad = function () {
    };
    HomePage.prototype.ngAfterContentInit = function () {
        //Called after ngOnInit when the component's or directive's content has been initialized.
        //Add 'implements AfterContentInit' to the class.
        this.loadMap();
    };
    HomePage.prototype.loadMap = function () {
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: -15.7745457, lng: -48.8093811 },
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            styles: [
                {
                    "elementType": "labels",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                }
            ]
        });
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], HomePage.prototype, "mapElement", void 0);
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map