import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, ModalController } from '@ionic/angular';
import { Doenca } from 'src/model/Doenca';
import { HttpClient } from '@angular/common/http';
import { Util } from 'src/util/Util';
import { ActivatedRoute } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ShowModalComponent } from '../show-modal/show-modal.component';
var DoencaPage = /** @class */ (function () {
    function DoencaPage(NavCtrl, http, alertController, splashScreen, modalController, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.http = http;
        this.alertController = alertController;
        this.splashScreen = splashScreen;
        this.modalController = modalController;
        this.route = route;
        this.doencas = new Array();
        this.route.params.subscribe(function (resp) {
            _this.reload();
        });
    }
    DoencaPage.prototype.visualizar = function (doenca) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: ShowModalComponent,
                            componentProps: {
                                'doenca': doenca
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DoencaPage.prototype.cadastrar = function () {
        this.NavCtrl.navigateForward('doenca/cadastrar');
    };
    DoencaPage.prototype.editar = function (id) {
        this.NavCtrl.navigateForward('doenca/cadastrar/' + id);
    };
    DoencaPage.prototype.reload = function () {
        var _this = this;
        this.doencas = new Array();
        this.http.get(Util.url + "/api/doenca/all").subscribe(function (resp) {
            console.log(resp);
            resp.forEach(function (doenca) {
                _this.doencas.push(new Doenca(doenca));
                //this.splashScreen.hide();
            });
        });
    };
    DoencaPage.prototype.deletar = function (id, nome) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Excluir!',
                            message: 'Deseja deletar "' + nome + '"? Após a Confirmação não é possível recuperar o registro.',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                    }
                                }, {
                                    text: 'Deletar',
                                    handler: function () {
                                        _this.http.delete(Util.url + "/api/doenca/remove/" + id).subscribe(function (resp) {
                                            _this.reload();
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DoencaPage = tslib_1.__decorate([
        Component({
            selector: 'app-doenca',
            templateUrl: './doenca.page.html',
            styleUrls: ['./doenca.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            HttpClient,
            AlertController,
            SplashScreen,
            ModalController,
            ActivatedRoute])
    ], DoencaPage);
    return DoencaPage;
}());
export { DoencaPage };
//# sourceMappingURL=doenca.page.js.map