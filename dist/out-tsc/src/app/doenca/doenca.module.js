import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { IonicModule } from '@ionic/angular';
import { DoencaPage } from './doenca.page';
import { HttpClientModule } from '@angular/common/http';
import { ShowModalModule } from '../show-modal/show-modal.module';
var routes = [
    {
        path: '',
        component: DoencaPage
    }
];
var DoencaPageModule = /** @class */ (function () {
    function DoencaPageModule() {
    }
    DoencaPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                HttpClientModule,
                ShowModalModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            providers: [SplashScreen],
            declarations: [DoencaPage]
        })
    ], DoencaPageModule);
    return DoencaPageModule;
}());
export { DoencaPageModule };
//# sourceMappingURL=doenca.module.js.map