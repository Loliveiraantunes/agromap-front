import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Doenca } from 'src/model/Doenca';
import { Cultura } from 'src/model/Cultura';
var ShowModalComponent = /** @class */ (function () {
    function ShowModalComponent(navParams, modal) {
        this.modal = modal;
        this.doenca = new Doenca(null);
        this.cultura = new Cultura(null);
        this.doenca = navParams.get('doenca');
        this.cultura = navParams.get('cultura');
    }
    ShowModalComponent.prototype.ngOnInit = function () {
    };
    ShowModalComponent.prototype.dismiss = function () {
        this.modal.dismiss();
    };
    ShowModalComponent = tslib_1.__decorate([
        Component({
            selector: 'app-show-modal',
            templateUrl: './show-modal.component.html',
            styleUrls: ['./show-modal.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavParams, ModalController])
    ], ShowModalComponent);
    return ShowModalComponent;
}());
export { ShowModalComponent };
//# sourceMappingURL=show-modal.component.js.map