import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { ShowModalComponent } from './show-modal.component';
var routes = [
    {
        path: '',
        component: ShowModalComponent
    }
];
var ShowModalModule = /** @class */ (function () {
    function ShowModalModule() {
    }
    ShowModalModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                HttpClientModule,
                ReactiveFormsModule
            ],
            declarations: [
                ShowModalComponent
            ],
            entryComponents: [
                ShowModalComponent
            ]
        })
    ], ShowModalModule);
    return ShowModalModule;
}());
export { ShowModalModule };
//# sourceMappingURL=show-modal.module.js.map