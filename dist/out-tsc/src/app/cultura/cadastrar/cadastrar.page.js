import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Cultura } from 'src/model/Cultura';
import { Util } from 'src/util/Util';
var CadastrarPage = /** @class */ (function () {
    function CadastrarPage(NavCtrl, formBuilder, toastController, activatedRoute, http, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.formBuilder = formBuilder;
        this.toastController = toastController;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.route = route;
        this.cultura = new Cultura(null);
        this.form = this.formBuilder.group({
            tipo: [''],
            nome: [''],
            descricao: ['']
        });
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        if (this.id != null) {
            this.http.get(Util.url + "/api/cultura/" + this.id).subscribe(function (resp) {
                _this.cultura = new Cultura(resp);
            });
        }
    }
    CadastrarPage.prototype.invalid = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Verifique se os campos estão preenchidos corretamente',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage.prototype.ngOnInit = function () {
    };
    CadastrarPage.prototype.salvar = function () {
        var _this = this;
        if (!this.form.valid) {
            this.invalid();
            return;
        }
        if (this.id == null) {
            this.cultura = new Cultura(this.form.value);
            this.http.post(Util.url + "/api/cultura/add", this.cultura).subscribe(function (resp) {
                _this.presentToast();
                _this.NavCtrl.navigateRoot('cultura');
            });
        }
        else {
            this.http.put(Util.url + "/api/cultura/update", this.cultura).subscribe(function (resp) {
                _this.update();
                _this.route.navigate(['/cultura']);
            });
        }
    };
    CadastrarPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Cultura cadastrada com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage.prototype.update = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Cultura atualizada com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CadastrarPage.prototype.back = function () {
        this.NavCtrl.back();
    };
    CadastrarPage = tslib_1.__decorate([
        Component({
            selector: 'app-cadastrar',
            templateUrl: './cadastrar.page.html',
            styleUrls: ['./cadastrar.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            FormBuilder,
            ToastController,
            ActivatedRoute,
            HttpClient,
            Router])
    ], CadastrarPage);
    return CadastrarPage;
}());
export { CadastrarPage };
//# sourceMappingURL=cadastrar.page.js.map