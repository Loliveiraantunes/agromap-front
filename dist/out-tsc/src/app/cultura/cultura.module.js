import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CulturaPage } from './cultura.page';
import { ShowModalModule } from '../show-modal/show-modal.module';
var routes = [
    {
        path: '',
        component: CulturaPage
    }
];
var CulturaPageModule = /** @class */ (function () {
    function CulturaPageModule() {
    }
    CulturaPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ShowModalModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CulturaPage]
        })
    ], CulturaPageModule);
    return CulturaPageModule;
}());
export { CulturaPageModule };
//# sourceMappingURL=cultura.module.js.map