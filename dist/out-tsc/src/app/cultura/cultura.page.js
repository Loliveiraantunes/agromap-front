import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, ModalController } from '@ionic/angular';
import { Cultura } from 'src/model/Cultura';
import { HttpClient } from '@angular/common/http';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ActivatedRoute } from '@angular/router';
import { ShowModalComponent } from '../show-modal/show-modal.component';
import { Util } from 'src/util/Util';
var CulturaPage = /** @class */ (function () {
    function CulturaPage(NavCtrl, http, alertController, splashScreen, modalController, route) {
        var _this = this;
        this.NavCtrl = NavCtrl;
        this.http = http;
        this.alertController = alertController;
        this.splashScreen = splashScreen;
        this.modalController = modalController;
        this.route = route;
        this.culturas = new Array();
        this.route.params.subscribe(function (resp) {
            _this.reload();
        });
    }
    CulturaPage.prototype.ngOnInit = function () {
    };
    CulturaPage.prototype.visualizar = function (cultura) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: ShowModalComponent,
                            componentProps: {
                                'cultura': cultura
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CulturaPage.prototype.editar = function (id) {
        this.NavCtrl.navigateForward('cultura/cadastrar/' + id);
    };
    CulturaPage.prototype.reload = function () {
        var _this = this;
        this.culturas = new Array();
        this.http.get(Util.url + "/api/cultura/all").subscribe(function (resp) {
            console.log(resp);
            resp.forEach(function (cultura) {
                _this.culturas.push(new Cultura(cultura));
                //this.splashScreen.hide();
            });
        });
    };
    CulturaPage.prototype.deletar = function (id, nome) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Excluir!',
                            message: 'Deseja deletar "' + nome + '"? Após a Confirmação não é possível recuperar o registro.',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                    }
                                }, {
                                    text: 'Deletar',
                                    handler: function () {
                                        _this.http.delete(Util.url + "/api/cultura/remove/" + id).subscribe(function (resp) {
                                            _this.reload();
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CulturaPage.prototype.cadastrar = function () {
        this.NavCtrl.navigateForward('cultura/cadastrar');
    };
    CulturaPage = tslib_1.__decorate([
        Component({
            selector: 'app-cultura',
            templateUrl: './cultura.page.html',
            styleUrls: ['./cultura.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            HttpClient,
            AlertController,
            SplashScreen,
            ModalController,
            ActivatedRoute])
    ], CulturaPage);
    return CulturaPage;
}());
export { CulturaPage };
//# sourceMappingURL=cultura.page.js.map