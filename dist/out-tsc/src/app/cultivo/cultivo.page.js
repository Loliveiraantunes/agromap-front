import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Cultura } from 'src/model/Cultura';
import { Cultivo } from 'src/model/Cultivo';
import { NavController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Util } from 'src/util/Util';
import { Propriedade } from 'src/model/Propriedade';
var CultivoPage = /** @class */ (function () {
    function CultivoPage(NavCtrl, formBuilder, toastController, activatedRoute, http, route) {
        this.NavCtrl = NavCtrl;
        this.formBuilder = formBuilder;
        this.toastController = toastController;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.route = route;
        this.culturas = new Array();
        this.cultivo = new Cultivo(null);
        this.propriedade = new Propriedade(null);
        this.form = this.formBuilder.group({
            cultura: ['', Validators.required],
            dataInicio: ['', Validators.required],
            dataFim: ['']
        });
    }
    CultivoPage.prototype.ngOnInit = function () {
        var _this = this;
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        this.idPropriedade = this.activatedRoute.snapshot.paramMap.get('idPropriedade');
        if (this.id != null) {
            this.http.get(Util.url + "/api/cultivo/" + this.id).subscribe(function (resp) {
                console.log(resp);
                _this.cultivo = new Cultivo(resp);
            });
        }
        this.http.get(Util.url + "/api/cultura/all").subscribe(function (resp) {
            resp.forEach(function (cultura) {
                _this.culturas.push(new Cultura(cultura));
            });
        });
        this.http.get(Util.url + "/api/propriedade/" + this.idPropriedade).subscribe(function (resp) {
            _this.propriedade = new Propriedade(resp);
        });
    };
    CultivoPage.prototype.salvar = function () {
        var _this = this;
        this.cultivo.propriedade = this.propriedade;
        if (!this.form.valid) {
            this.invalid();
            return;
        }
        if (this.id == null) {
            this.cultivo.propriedade = this.propriedade;
            this.http.post(Util.url + "/api/cultivo/add", this.cultivo).subscribe(function (resp) {
                _this.presentToast();
                _this.NavCtrl.navigateRoot('propriedade/visualizar/' + _this.idPropriedade);
            });
        }
        else {
            this.http.put(Util.url + "/api/cultivo/update", this.cultivo).subscribe(function (resp) {
                _this.update();
                _this.NavCtrl.navigateRoot('propriedade/visualizar/' + _this.idPropriedade);
            });
        }
    };
    CultivoPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Cadastrada com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CultivoPage.prototype.invalid = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Verifique se os campos estão preenchidos corretamente',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CultivoPage.prototype.update = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Atualizado com Sucesso',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CultivoPage.prototype.back = function () {
        this.NavCtrl.back();
    };
    CultivoPage.prototype.compareById = function (o1, o2) {
        return o1.id === o2.id;
    };
    CultivoPage = tslib_1.__decorate([
        Component({
            selector: 'app-cultivo',
            templateUrl: './cultivo.page.html',
            styleUrls: ['./cultivo.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            FormBuilder,
            ToastController,
            ActivatedRoute,
            HttpClient,
            Router])
    ], CultivoPage);
    return CultivoPage;
}());
export { CultivoPage };
//# sourceMappingURL=cultivo.page.js.map