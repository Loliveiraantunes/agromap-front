import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CultivoPage } from './cultivo.page';
import { HttpClientModule } from '@angular/common/http';
var routes = [
    {
        path: '',
        component: CultivoPage
    }
];
var CultivoPageModule = /** @class */ (function () {
    function CultivoPageModule() {
    }
    CultivoPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CultivoPage]
        })
    ], CultivoPageModule);
    return CultivoPageModule;
}());
export { CultivoPageModule };
//# sourceMappingURL=cultivo.module.js.map