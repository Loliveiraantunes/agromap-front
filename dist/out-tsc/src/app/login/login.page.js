import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuController, ModalController, ToastController, NavController } from '@ionic/angular';
import { Util } from 'src/util/Util';
import { User } from 'src/model/User';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CadastrarModalComponent } from '../cadastrar-modal/cadastrar-modal.component';
var LoginPage = /** @class */ (function () {
    function LoginPage(menuCtrl, formBuilder, http, modalController, toastController, navCtrl) {
        this.menuCtrl = menuCtrl;
        this.formBuilder = formBuilder;
        this.http = http;
        this.modalController = modalController;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.form = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    LoginPage.prototype.showModal = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: CadastrarModalComponent
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Logado.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.error404 = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'usuário ou senha incorreta.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.submit = function () {
        var _this = this;
        this.user = new User(this.form.value);
        this.user.login = this.form.value.email;
        this.http.post(Util.url + "/api/user/login", this.user).subscribe(function (resp) {
            var userJS = new User(resp);
            Util.loggin(userJS);
            _this.presentToast();
            _this.navCtrl.navigateRoot('/home');
            _this.menuCtrl.enable(true);
        }, function (error) {
            _this.error404();
        });
    };
    LoginPage.prototype.ngOnInit = function () {
        this.menuCtrl.enable(false);
    };
    LoginPage = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuController,
            FormBuilder,
            HttpClient,
            ModalController,
            ToastController,
            NavController])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map